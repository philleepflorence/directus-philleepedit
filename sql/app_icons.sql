# Create Table

CREATE TABLE `app_icons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL COMMENT 'Name of Icon',
  `class` varchar(100) DEFAULT NULL COMMENT 'See Usage: @ https://materialdesignicons.com/',
  `path` varchar(100) DEFAULT NULL COMMENT 'Dot syntax path to icon, e.g. navigation.about for the Navigation About Button(s) - used when creating icons object',
  `color` varchar(7) DEFAULT NULL COMMENT 'Hexadecimal color of icon - useful for Social Media Icons that must be a certain color, e.g. Facebook: #3b5998',
  PRIMARY KEY (`id`),
  UNIQUE KEY `NAME` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
    ('app_icons', 'id', 'INT', 'primary_key', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL),
    ('app_icons', 'name', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 'Name of Icon - May be displayed as a hint if not used as a Navigation Icon', NULL),
    ('app_icons', 'class', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 2, 'See Usage: @ https://materialdesignicons.com/', NULL),
    ('app_icons', 'path', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 3, 'Dot syntax path to icon, e.g. navigation.about for the Navigation About Button(s) - used when creating icons object', NULL),
    ('app_icons', 'color', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 4, 'Hexadecimal color of icon - useful for Social Media Icons that must be a certain color, e.g. Facebook: #3b5998', NULL);

# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
    (1, 'app_icons', NULL, 'name,class,path,color', 'id', 'ASC', '1,2', NULL, '{\"spacing\":\"cozy\",\"last_updated\":true,\"show_footer\":false,\"item_numbers\":false,\"currentView\":\"table\",\"revisions_count\":false,\"table\":{\"show_footer\":false,\"item_numbers\":false,\"last_updated\":true},\"tiles\":{\"subtitle_column\":\"title\"},\"subtitle_column\":\"title\",\"type_column\":\"title\",\"title_column\":\"title\",\"comments_count\":false}');

# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
    ('app_icons', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
    ('app_icons', 2, 0, 0, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_icons', '{{name}} - {{class}}', '/api/preview/?preview=true&table=app_icons', 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
