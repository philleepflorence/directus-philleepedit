# Create Table

CREATE TABLE `app_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section` varchar(50) DEFAULT 'application' COMMENT 'Select section configuration belongs - defaults to application',
  `key` varchar(100) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL COMMENT 'Short Description - useful for admins making updates',
  `value` text,
  `params` text,
  `public` tinyint(1) DEFAULT '0' COMMENT 'Make configuration public (use in browser) - by default all are private,',
  `section_basic` varchar(1) DEFAULT NULL COMMENT 'Basic Information',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Insert default rows

INSERT INTO `app_configuration` (`section`, `key`, `description`, `value`, `params`, `public`)
VALUES
	('application', 'email', 'Site Email - Usually displayed as the \'From\' Email when sending out emails from the Application', '', NULL, 1),
	('application', 'name', 'Name of site or application', '', NULL, 1),
	('application', 'logo', 'Main Logo for Application or Site', '', NULL, 1),
	('application', 'favicon', 'Main Icon for Application', '', NULL, 1),
	('application', 'timezone', 'Company Timezone: See - https://www.w3schools.com/php/php_ref_timezones.asp', 'America/New_York', NULL, 1),
	('application', 'tagline', 'A catchphrase or slogan, especially as used in advertising', '', NULL, 1),
	('application', 'description', 'Short description of project or organization', '', NULL, 1),
	('images', 'resize.url', 'CDN URL for resizing images', '', NULL, 1),
	('date', 'format.date', 'Format to use for Dates - https://momentjs.com/docs/#/parsing/string-format/', 'MMMM Do, YYYY', NULL, 1),
	('date', 'format.day.text', 'Format to use for Days - https://momentjs.com/docs/#/parsing/string-format/', 'dddd', NULL, 1),
	('date', 'format.time', 'Format to use for Time - https://momentjs.com/docs/#/parsing/string-format/', 'hh:mma', NULL, 1),
	('date', 'format.month', 'Format to use for Month - https://momentjs.com/docs/#/parsing/string-format/', 'MMM', NULL, 1),
	('date', 'format.day.number', 'Format to use for Days - https://momentjs.com/docs/#/parsing/string-format/', 'DD', NULL, 1),
	('maps', 'google.key', 'Google Maps API Key', '', NULL, 1),
	('maps', 'google.styles.lightgray', 'Minified Custom map style - See: https://snazzymaps.com/', '[{\"featureType\":\"water\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#e9e9e9\"},{\"lightness\":17}]},{\"featureType\":\"landscape\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#f5f5f5\"},{\"lightness\":20}]},{\"featureType\":\"road.highway\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#ffffff\"},{\"lightness\":17}]},{\"featureType\":\"road.highway\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#ffffff\"},{\"lightness\":29},{\"weight\":0.2}]},{\"featureType\":\"road.arterial\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#ffffff\"},{\"lightness\":18}]},{\"featureType\":\"road.local\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#ffffff\"},{\"lightness\":16}]},{\"featureType\":\"poi\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#f5f5f5\"},{\"lightness\":21}]},{\"featureType\":\"poi.park\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#dedede\"},{\"lightness\":21}]},{\"elementType\":\"labels.text.stroke\",\"stylers\":[{\"visibility\":\"on\"},{\"color\":\"#ffffff\"},{\"lightness\":16}]},{\"elementType\":\"labels.text.fill\",\"stylers\":[{\"saturation\":36},{\"color\":\"#333333\"},{\"lightness\":40}]},{\"elementType\":\"labels.icon\",\"stylers\":[{\"visibility\":\"off\"}]},{\"featureType\":\"transit\",\"elementType\":\"geometry\",\"stylers\":[{\"color\":\"#f2f2f2\"},{\"lightness\":19}]},{\"featureType\":\"administrative\",\"elementType\":\"geometry.fill\",\"stylers\":[{\"color\":\"#fefefe\"},{\"lightness\":20}]},{\"featureType\":\"administrative\",\"elementType\":\"geometry.stroke\",\"stylers\":[{\"color\":\"#fefefe\"},{\"lightness\":17},{\"weight\":1.2}]}]', NULL, 1),
	('maps', 'google.marker.image', 'Image to use as Google Map Pin.', '', NULL, 1),
	('maps', 'location', 'Address for the main location to use in maps', '', NULL, 1),
	('maps', 'google.marker.width', 'Width of Google Marker Image', '', NULL, 1),
	('maps', 'google.marker.height', 'Height of Google Marker Image', '', NULL, 1),
	('maps', 'google.zoom', 'Zoom level of Google Maps 1 - 18', '13', NULL, 1),
	('form', 'recipient.contact', 'Email to which contact form data will go in addition to being stored in App Forms', '', NULL, NULL),
	('mail', 'outgoing.host', 'Outgoing Mail (SMTP) Server', '', NULL, NULL),
	('mail', 'outgoing.port', 'Outgoing Mail (SMTP) Server Port - Requires SSL/TLS/STARTTLS', '', NULL, NULL),
	('mail', 'outgoing.password', 'Outgoing Mail (SMTP) Server Password - Required if authenticating', ' ', NULL, NULL),
	('analytics', 'google.id', 'Google Analytics Tracking ID', '', NULL, NULL),
	('mail', 'outgoing.user', 'Outgoing Mail (SMTP) Server Username', '', NULL, NULL),
	('mail', 'outgoing.service', 'Outgoing Mail (SMTP) Service: See list of mail services in how to guide', '', NULL, NULL),
	('mail', 'outgoing.signature', 'Footer or signature of all outgoing mail', '', NULL, NULL);


# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_configuration', 'description', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 4, 'Short Description of configuration entry - useful for admins making updates', '{\"id\":\"text_input\"}'),
	('app_configuration', 'id', NULL, 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, '', NULL),
	('app_configuration', 'key', NULL, 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 2, 'Dot syntax Unique Key Identifier', NULL),
	('app_configuration', 'params', NULL, 'textarea', NULL, NULL, NULL, NULL, NULL, 0, 0, 7, 'Configuration item additional parameters or options', NULL),
	('app_configuration', 'public', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 6, 'Make configuration public (use in browser) - by default all are private', NULL),
	('app_configuration', 'section', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 5, 'Select section configuration belongs - defaults to application', '{\"id\":\"text_input\",\"placeholder\":\"application\"}'),
	('app_configuration', 'section_basic', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 'Basic Information', '{\"id\":\"section_break\",\"title\":\"Basic Information\",\"instructions\":\"<p>Settings and options for the entire application or website.</p>\"}'),
	('app_configuration', 'value', NULL, 'textarea', NULL, NULL, NULL, NULL, NULL, 0, 1, 3, 'Configuration item value', NULL);

# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
	(1, 'app_configuration', NULL, 'section,key,value,public', 'id', 'ASC', '1,2', '', '{\"spacing\":\"cozy\",\"last_updated\":true,\"item_numbers\":false,\"currentView\":\"table\",\"show_footer\":false,\"revisions_count\":false,\"table\":{\"show_footer\":false,\"item_numbers\":false,\"last_updated\":true},\"tiles\":{\"subtitle_column\":\"title\"},\"subtitle_column\":\"title\",\"type_column\":\"title\",\"title_column\":\"title\",\"comments_count\":false}');

# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
	('app_configuration', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
	('app_configuration', 2, 1, 2, 2, 1, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_configuration', '{{section}}.{{key}} - {{value}} ', '/api/preview/?preview=true&table=app_configuration', 0, 0, 1, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
