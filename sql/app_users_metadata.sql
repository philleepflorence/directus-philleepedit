# Create Table

CREATE TABLE `app_users_metadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '2',
  `key` varchar(50) DEFAULT NULL,
  `value` text,
  `user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_users_metadata', 'id', 'INT', 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL),
	('app_users_metadata', 'key', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 3, '', NULL),
	('app_users_metadata', 'status', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, NULL, NULL),
	('app_users_metadata', 'user', 'INT', 'many_to_one', 'MANYTOONE', 'app_users', NULL, NULL, 'user', 0, 1, 2, '', '{\"id\":\"many_to_one\",\"visible_column\":\"first_name,last_name,email\",\"visible_column_template\":\"{{first_name}} {{last_name}} - {{email}}\",\"placeholder\":\"Please select App User\",\"filter_column\":\"first_name\"}'),
	('app_users_metadata', 'value', 'TEXT', 'textarea', NULL, NULL, NULL, NULL, NULL, 0, 1, 4, '', NULL);

    
# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
	(1, 'app_users_metadata', NULL, '', 'id', 'ASC', '1,2', NULL, NULL);
    
# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
	('app_users_metadata', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
	('app_users_metadata', 2, 1, 2, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_users_metadata', '', '', 1, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);