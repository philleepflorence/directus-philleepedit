# Create Table

CREATE TABLE `app_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(1) NOT NULL DEFAULT '1',
  `name` varchar(50) DEFAULT NULL COMMENT 'Unique name or identifier of page.',
  `path` varchar(100) DEFAULT NULL,
  `headline` varchar(100) DEFAULT NULL COMMENT 'Page Headline - may be different from Page Title, which may vary by page',
  `title` varchar(150) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL COMMENT 'Page Description',
  `image` int(11) DEFAULT NULL COMMENT 'Metadata image - useful for sharing on social media - leave blank to use image from page data',
  `content_image` tinyint(1) DEFAULT NULL COMMENT 'Use image(s) from content (blog, listings, et al) if available, will default to image if added',
  `sort` int(11) DEFAULT '0',
  `section_basic` varchar(100) DEFAULT NULL COMMENT 'Basic Information',
  `section_display` varchar(100) DEFAULT NULL COMMENT 'Displayed Information',
  `synopsis` text COMMENT 'Short synopsis of the page - should be one paragraph',
  `contents` text COMMENT 'Contents of the page - can be multiple paragraphs',
  PRIMARY KEY (`id`),
  UNIQUE KEY `NAME` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_pages', 'active', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 'Active status - Only active rows will be displayed', NULL),
	('app_pages', 'contents', 'TEXT', 'wysiwyg', NULL, NULL, NULL, NULL, NULL, 0, 0, 11, 'Contents of the page - can be multiple paragraphs', NULL),
	('app_pages', 'content_image', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 13, 'Use image(s) from content (blog, listings, et al) if available, will default to image if added', NULL),
	('app_pages', 'description', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 7, 'Page Description - To use a property, use :property, e.g. :name', NULL),
	('app_pages', 'headline', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 9, 'Page Headline - may be different from Page Title, which may vary by page. Only needed if page does not have a table.', '{\"id\":\"text_input\"}'),
	('app_pages', 'id', NULL, 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, '', NULL),
	('app_pages', 'image', 'INT', 'single_file', 'MANYTOONE', 'directus_files', NULL, NULL, 'image', 0, 0, 12, 'Metadata image - useful for sharing on social media. Also used as the Hero or Campaign image.', '{\"id\":\"single_file\",\"allowed_filetypes\":\"jpg,jpeg,gif,png,svg\"}'),
	('app_pages', 'name', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 4, 'Unique name or identifier of page.', NULL),
	('app_pages', 'path', NULL, 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 5, 'Internal URL Path', NULL),
	('app_pages', 'section_basic', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 'Basic Information', '{\"id\":\"section_break\",\"title\":\"Basic Information\",\"instructions\":\"<p>Settings, options, and metadata of all static and dynamic pages. The Site Map is created from here along with the <em>App Navigation</em>. The following are all required and are used as metadata for the Page.</p>\"}'),
	('app_pages', 'section_display', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 8, 'Displayed Information', '{\"id\":\"section_break\",\"title\":\"Displayed Information\",\"instructions\":\"<p>Enter as many of the following as possible. Headlines, image, and synopsis are usually displayed on the page. The image is also used when sharing the page on Social Media and other sites.</p>\"}'),
	('app_pages', 'sort', 'INT', 'sort', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, '', NULL),
	('app_pages', 'synopsis', 'TEXT', 'wysiwyg', NULL, NULL, NULL, NULL, NULL, 0, 0, 10, 'Short synopsis of the page - should be one paragraph', NULL),
	('app_pages', 'title', NULL, 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 6, 'Page Title', NULL);

# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
    (1, 'app_pages', NULL, 'name,headline,path,title,image,sort', 'sort', 'ASC', '1,2', NULL, '{\"spacing\":\"cozy\",\"last_updated\":true,\"show_footer\":false,\"item_numbers\":false,\"currentView\":\"table\",\"revisions_count\":false,\"table\":{\"show_footer\":false,\"item_numbers\":false,\"last_updated\":true},\"tiles\":{\"subtitle_column\":\"description\",\"type_column\":\"title\",\"title_column\":\"title\"},\"subtitle_column\":\"title\",\"type_column\":\"title\",\"title_column\":\"title\",\"comments_count\":false}');

# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
    ('app_pages', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
    ('app_pages', 2, 0, 0, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_pages', '{{name}} - {{path}}', '/api/preview/?preview=true&id={{id}}&table=app_pages', 0, 0, 1, 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);