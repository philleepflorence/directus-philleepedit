# Create Table

CREATE TABLE `app_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '2',
  `first_name` varchar(50) DEFAULT NULL COMMENT 'First Name of subscriber',
  `last_name` varchar(50) DEFAULT NULL COMMENT 'Last Name of subscriber',
  `email` varchar(128) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL COMMENT 'Username of subscriber',
  `password` varchar(255) DEFAULT NULL COMMENT 'Password of subscriber',
  `salt` varchar(100) DEFAULT NULL COMMENT 'System generated hash',
  `location` varchar(100) DEFAULT NULL COMMENT 'Location of subscriber',
  `bio` text COMMENT 'Biography or profile of subscriber',
  `telephone` varchar(15) DEFAULT NULL COMMENT 'Mobile phone number of subscriber',
  `section_basic` varchar(100) DEFAULT NULL COMMENT 'Basic Information',
  PRIMARY KEY (`id`),
  UNIQUE KEY `EMAIL` (`email`),
  KEY `USERNAME` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_users', 'bio', 'TEXT', 'wysiwyg', NULL, NULL, NULL, NULL, NULL, 0, 0, 11, '', NULL),
	('app_users', 'email', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 5, '', '{\"id\":\"text_input\"}'),
	('app_users', 'first_name', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, '', NULL),
	('app_users', 'id', 'INT', 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL),
	('app_users', 'last_name', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 4, '', NULL),
	('app_users', 'location', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 10, '', NULL),
	('app_users', 'password', 'VARCHAR', 'password', NULL, NULL, NULL, NULL, NULL, 0, 0, 8, '', NULL),
	('app_users', 'salt', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 9, '', NULL),
	('app_users', 'section_basic', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 'Basic Information', '{\"id\":\"section_break\",\"title\":\"Basic Information\",\"instructions\":\"<p>Collection of public application users or subscribers. These typically are not the same as the administrative users that can log in to the CMS or API.</p>\"}'),
	('app_users', 'status', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, NULL, NULL),
	('app_users', 'telephone', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 7, '', NULL),
	('app_users', 'username', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 6, '', NULL);
    
# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
	(1, 'app_users', NULL, 'first_name,last_name,email,username', 'id', 'ASC', '1,2', NULL, NULL);
    
# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
	('app_users', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
	('app_users', 2, 1, 2, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_users', '{{first_name}} {{last_name}} - {{email}}', '/api/preview/?preview=true&id={{id}}&table=app_users', 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);