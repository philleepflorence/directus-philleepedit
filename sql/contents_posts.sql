# Create Table

CREATE TABLE `contents_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '2',
  `sort` int(11) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL COMMENT 'Feature post in various sections of application',
  `post_type` int(11) DEFAULT NULL COMMENT 'Type of Post - Main Category of Post',
  `title` varchar(150) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `content` text COMMENT 'Post Contents or Details',
  `synopsis` text,
  `path` varchar(250) DEFAULT NULL COMMENT 'Internal Permalink - may have prefix added later by system',
  `comments` tinyint(4) DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `unpublish_date` datetime DEFAULT '3000-01-01 00:00:00' COMMENT 'Date to remove post from site - if blank or default, post will always be displayed',
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `section_break_basic_info` varchar(1) DEFAULT NULL COMMENT 'This info will also appear in Feeds and any ads created for this post',
  `section_break_details` varchar(1) DEFAULT NULL COMMENT 'Let people know what type of post this is and its details',
  `section_break_metadata` varchar(1) DEFAULT NULL COMMENT 'Basic metadata and configuration of post',
  `image` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `PATH` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('contents_posts', 'categories', 'ALIAS', 'many_to_many', 'MANYTOMANY', 'app_tags', 'contents_posts_categories', 'post_id', 'tag_id', 0, 0, 16, 'Post Sub Categories - optional if you wish to add more categories other than the Post Type', '{\"id\":\"many_to_many\",\"visible_column_template\":\"{{name}} ({{tag_type}})\",\"visible_columns\":\"name,path,tag_type\",\"min_entries\":\"1\",\"no_duplicates\":1,\"filter_column\":\"name\"}'),
	('contents_posts', 'comments', NULL, 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 18, 'Allow comments', NULL),
	('contents_posts', 'content', 'TEXT', 'wysiwyg', NULL, NULL, NULL, NULL, NULL, 0, 1, 14, 'Post Contents or Details', '{\"id\":\"wysiwyg_full\",\"headings\":\",h3,h4,h5,h6,\",\"headings_h5\":\"h5\",\"headings_h6\":\"h6\",\"inline\":\",bold,italic,underline,superscript,subscript,\",\"inline_superscript\":\"superscript\",\"inline_subscript\":\"subscript\",\"inline_bold\":\"bold\",\"read_only\":\"0\",\"show_element_path\":\"1\",\"custom_toolbar_options\":\"\",\"custom_wrapper\":\"\",\"max_height\":\"500\"}'),
	('contents_posts', 'description', NULL, 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 11, 'Short Description of Post', NULL),
	('contents_posts', 'featured', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 4, 'Feature post in various sections of application', NULL),
	('contents_posts', 'id', NULL, 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, '', NULL),
	('contents_posts', 'image', 'INT', 'single_file', 'MANYTOONE', 'directus_files', NULL, NULL, 'image', 0, 0, 15, '', '{\"id\":\"single_file\"}'),
	('contents_posts', 'path', 'VARCHAR', 'slug', NULL, NULL, NULL, NULL, NULL, 0, 0, 19, 'Internal Permalink - may have prefix added later by system', '{\"id\":\"slug\",\"read_only\":1,\"mirrored_field\":\"title\"}'),
	('contents_posts', 'post_author', 'ALIAS', 'many_to_many', 'MANYTOMANY', 'directus_users', 'contents_posts_users', 'post_id', 'user_id', 0, 1, 9, 'Authors and Contributors', '{\"id\":\"many_to_many\",\"filter_column\":\"first_name\",\"visible_columns\":\"first_name,last_name,position\",\"visible_column_template\":\"{{first_name}} {{last_name}} - {{position}}\",\"min_entries\":\"1\",\"add_button\":\"1\",\"choose_button\":\"1\",\"remove_button\":\"1\",\"filter_type\":\"dropdown\",\"no_duplicates\":\"1\"}'),
	('contents_posts', 'post_type', 'INT', 'many_to_one', 'MANYTOONE', 'app_tags', NULL, NULL, 'post_type', 0, 1, 5, 'Type of Post - Main Category of Post', '{\"id\":\"many_to_one\",\"visible_column_template\":\"{{name}} ({{tag_type}})\",\"visible_column\":\"name,path,tag_type\",\"filter_column\":\"name\"}'),
	('contents_posts', 'publish_date', NULL, 'datetime', NULL, NULL, NULL, NULL, NULL, 0, 1, 6, 'Date to publish post', NULL),
	('contents_posts', 'section_break_basic_info', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 8, 'This info will also appear in Feeds and any ads created for this post', '{\"id\":\"section_break\",\"instructions\":\"<p>This info will also appear in Feeds and any ads created for this post</p>\",\"title\":\"Basic Information\",\"show_inline\":0}'),
	('contents_posts', 'section_break_details', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 13, 'Let people know what type of post this is and its details', '{\"id\":\"section_break\",\"instructions\":\"<p>Let people know what type of post this is and its details</p>\",\"title\":\"Details\",\"show_inline\":0}'),
	('contents_posts', 'section_break_metadata', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 'Basic metadata and configuration of post', '{\"id\":\"section_break\",\"instructions\":\"<p>Basic metadata and configuration of post</p>\",\"title\":\"Post Configuration\",\"show_inline\":0}'),
	('contents_posts', 'sort', 'INT', 'sort', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, NULL, NULL),
	('contents_posts', 'status', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 'Active status - Only active rows will be displayed', NULL),
	('contents_posts', 'synopsis', NULL, 'wysiwyg', NULL, NULL, NULL, NULL, NULL, 0, 0, 12, 'Synopsis or Excerpt of Post', NULL),
	('contents_posts', 'tags', 'ALIAS', 'many_to_many', 'MANYTOMANY', 'app_tags', 'contents_posts_tags', 'post_id', 'tag_id', 0, 0, 17, 'Categories and Tags', '{\"id\":\"many_to_many\",\"visible_column_template\":\"{{name}} ({{tag_type}})\",\"visible_columns\":\"name,path,tag_type\",\"filter_column\":\"name\",\"min_entries\":\"1\",\"add_button\":\"1\",\"choose_button\":\"1\",\"remove_button\":\"1\",\"filter_type\":\"dropdown\",\"no_duplicates\":\"1\"}'),
	('contents_posts', 'title', NULL, 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 10, 'Post Title', NULL),
	('contents_posts', 'unpublish_date', 'DATETIME', 'datetime', NULL, NULL, NULL, NULL, NULL, 0, 0, 7, 'Date to remove post from site - if blank or default, post will always be displayed', '{\"id\":\"datetime\",\"contextual_date_in_listview\":1,\"include_seconds\":0,\"auto-populate_when_hidden_and_null\":1}'),
	('contents_posts', 'update_date', NULL, 'datetime', NULL, NULL, NULL, NULL, NULL, 1, 0, 20, 'Date post was updated', NULL);

# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
    (1, 'contents_posts', NULL, 'title,post_type,publish_date,featured,image', 'id', 'ASC', '1,2', '', '{\"spacing\":\"cozy\",\"show_footer\":false,\"item_numbers\":false,\"last_updated\":false,\"currentView\":\"table\",\"revisions_count\":false,\"table\":{\"show_footer\":false,\"item_numbers\":false,\"last_updated\":false},\"tiles\":{\"subtitle_column\":\"title\"},\"subtitle_column\":\"title\",\"type_column\":\"title\",\"title_column\":\"title\",\"comments_count\":false}');

# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
    ('contents_posts', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
    ('contents_posts', 2, 0, 0, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('contents_posts', '', '/api/preview/?preview=true&id={{id}}&table=contents_posts', 0, 0, 1, 0, NULL, 'id', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);