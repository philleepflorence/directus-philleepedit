# Create Table

CREATE TABLE `app_albums_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(10) unsigned DEFAULT NULL,
  `album_id` int(11) DEFAULT NULL COMMENT 'ID of Album - Links to App Albums',
  `file_id` int(11) DEFAULT NULL COMMENT 'ID of File - Links to Directus Files',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_albums_files', 'album_id', 'INT', 'numeric', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 'ID of Album - Links to App Albums', NULL),
	('app_albums_files', 'file_id', 'INT', 'numeric', NULL, NULL, NULL, NULL, NULL, 0, 0, 4, 'ID of File - Links to Directus Files', NULL),
	('app_albums_files', 'id', 'INT', 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL),
	('app_albums_files', 'sort', 'INT', 'sort', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, NULL, NULL),
	('app_albums_files', 'status', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 1, 0, 2, '', '{\"id\":\"status\"}');
    
# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
	(1, 'app_albums_files', NULL, 'album_id,file_id', 'sort', 'ASC', '1,2', NULL, NULL);
    
# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
	('app_albums_files', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
	('app_albums_files', 2, 1, 2, 2, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_albums_files', '', '', 1, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
