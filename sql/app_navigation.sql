# Create Table

CREATE TABLE `app_navigation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL DEFAULT '2',
  `name` varchar(50) DEFAULT NULL,
  `path` varchar(150) DEFAULT NULL COMMENT 'Internal Path',
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL COMMENT 'Short description',
  `parent` int(11) DEFAULT NULL COMMENT 'Parent Navigation - Appears under or as a dropdown of Parent',
  `section` varchar(50) DEFAULT NULL,
  `public` tinyint(1) DEFAULT NULL COMMENT 'Display to non authenticated users',
  `internal` tinyint(1) DEFAULT NULL COMMENT 'Links to an internal section OR page',
  `header` tinyint(1) DEFAULT NULL COMMENT 'Display in app or site header',
  `navigation` tinyint(4) DEFAULT NULL COMMENT 'Display IN app OR site main navigation',
  `footer` tinyint(4) DEFAULT NULL COMMENT 'Display IN app OR site footer',
  `private` tinyint(1) DEFAULT NULL COMMENT 'Display only TO authenticated users',
  `sort` int(11) DEFAULT NULL,
  `authentication` tinyint(1) DEFAULT NULL COMMENT 'Display in app or site authentication pages',
  `header_tablet` tinyint(1) DEFAULT NULL COMMENT 'Display in app or site header on Tablet Devices',
  `header_mobile` tinyint(1) DEFAULT NULL COMMENT 'Display in app or site header on Mobile Devices',
  `icon` int(11) DEFAULT NULL COMMENT 'Icon to use in template or layout - See App Icons for more information',
  `section_basic` varchar(1) DEFAULT NULL COMMENT 'Basic Information',
  `section_options` varchar(1) DEFAULT NULL COMMENT 'Additional options',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_navigation', 'active', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 'Active status - Only active rows will be displayed', NULL),
	('app_navigation', 'authentication', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 15, 'Display in app or site authentication pages', '{\"id\":\"toggle\"}'),
	('app_navigation', 'description', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 7, 'Short description to display on anchor tag (link)', '{\"id\":\"text_input\"}'),
	('app_navigation', 'footer', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 20, 'Display in app or site footer', '{\"id\":\"toggle\"}'),
	('app_navigation', 'header', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 16, 'Display in app or site header on Standard Devices', '{\"id\":\"toggle\"}'),
	('app_navigation', 'header_mobile', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 17, 'Display in app or site header on Mobile Devices. You must select Header also!', '{\"id\":\"toggle\"}'),
	('app_navigation', 'header_tablet', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 18, 'Display in app or site header on Tablet Devices. You must select Header also!', '{\"id\":\"toggle\"}'),
	('app_navigation', 'icon', 'INT', 'many_to_one', 'MANYTOONE', 'app_icons', NULL, NULL, 'icon', 0, 0, 10, 'Icon to use in template or layout - See App Icons for more information', '{\"id\":\"many_to_one\",\"visible_column\":\"name, oath\",\"visible_column_template\":\"{{name}}\",\"allow_null\":1,\"filter_column\":\"name\",\"filter_type\":\"textinput\"}'),
	('app_navigation', 'id', NULL, 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, '', NULL),
	('app_navigation', 'internal', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 12, 'Links to an internal section or page', '{\"id\":\"toggle\"}'),
	('app_navigation', 'name', NULL, 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 4, 'Name of Navigation', NULL),
	('app_navigation', 'navigation', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 19, 'Display in app or site main navigation', '{\"id\":\"toggle\"}'),
	('app_navigation', 'parent', 'INT', 'many_to_one', 'MANYTOONE', 'app_navigation', NULL, NULL, 'parent', 0, 0, 8, 'Parent Navigation - Appears under or as a dropdown of Parent', '{\"id\":\"many_to_one\",\"visible_column\":\"title,path,name\",\"visible_column_template\":\"{{title}}\",\"placeholder_text\":\"Select Parent - Optional\",\"filter_column\":\"title\",\"readonly\":\"0\",\"visible_status_ids\":\"1\",\"allow_null\":\"1\",\"filter_type\":\"dropdown\"}'),
	('app_navigation', 'path', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 5, 'Internal Path or External URL', '{\"id\":\"text_input\"}'),
	('app_navigation', 'private', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 14, 'Display only to authenticated users', '{\"id\":\"toggle\"}'),
	('app_navigation', 'public', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 13, 'Display to non authenticated users', '{\"id\":\"toggle\"}'),
	('app_navigation', 'section', NULL, 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 9, 'Navigation Headline - Useful In Navigation AND Footer Modules', NULL),
	('app_navigation', 'section_basic', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 'Basic Information', '{\"id\":\"section_break\",\"title\":\"Basic Information\",\"instructions\":\"<p>Settings, options, and contents of application navigation elements (links). The Site Map is created from here along with the <em>App Pages</em>. These control what is displayed and/or visible in the application navigation elements.</p>\"}'),
	('app_navigation', 'section_options', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 20, 'Additional options', '{\"id\":\"section_break\",\"title\":\"Additional options\",\"instructions\":\"<p>These options control how and where the navigation elements are displayed. For example, you can have a navigation element displayed in the header of the application but not displayed in the footer, and vice versa.</p>\"}'),
	('app_navigation', 'sort', 'INT', 'sort', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, '', NULL),
	('app_navigation', 'title', NULL, 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 6, 'Title To display', NULL);

# Update directus Preferences

INSERT INTO `directus_preferences` (`id`, `user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
    ('', 1, 'app_navigation', NULL, 'name,path,title,description,active', 'sort', 'ASC', '1,2', NULL, NULL);

# Update Directus Privileges

INSERT INTO `directus_privileges` (`id`, `table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
    ('', 'app_navigation', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
    ('', 'app_navigation', 2, 0, 0, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_navigation', '{{name}} - {{path}}', '/api/preview/?preview=true&table=app_navigation', 0, 0, 1, 0, NULL, 'id', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);