# Create Table

CREATE TABLE `app_albums` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '2',
  `sort` int(10) unsigned NOT NULL,
  `title` varchar(100) DEFAULT NULL COMMENT 'Title of Album',
  `description` varchar(200) DEFAULT NULL COMMENT 'Description - you can include location information here as well',
  `page` int(11) DEFAULT NULL COMMENT 'Page to display album and images',
  `section_basic` varchar(1) DEFAULT NULL COMMENT 'Basic Information',
  PRIMARY KEY (`id`),
  KEY `UNIQUES` (`title`,`page`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_albums', 'description', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 5, 'Description - you can include location information here as well', '{\"id\":\"text_input\"}'),
	('app_albums', 'id', 'INT', 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL),
	('app_albums', 'media', 'ALIAS', 'multiple_files', 'MANYTOMANY', 'directus_files', 'app_albums_files', 'album_id', 'file_id', 0, 1, 7, 'Images or Videos in Album - Must select at least one image or video. Avoid mixing videos and images in the same album.', '{\"id\":\"multiple_files\"}'),
	('app_albums', 'page', 'INT', 'many_to_one', 'MANYTOONE', 'app_pages', NULL, NULL, 'page', 0, 0, 6, 'Page to display album and images', '{\"id\":\"many_to_one\",\"filter_column\":\"name\",\"placeholder\":\"Please select a page\",\"visible_column_template\":\"{{name}} - {{path}}\",\"visible_column\":\"name,path\"}'),
	('app_albums', 'section_basic', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 'Basic Information', '{\"id\":\"section_break\",\"title\":\"Basic Information\",\"instructions\":\"<p>Collection of images grouped by a common name or theme. Can be included on one or more pages or sections.</p>\"}'),
	('app_albums', 'sort', 'INT', 'sort', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, NULL, NULL),
	('app_albums', 'status', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, NULL, NULL),
	('app_albums', 'title', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 4, 'Title of Album', NULL);
    
# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
	(1, 'app_albums', NULL, 'title,description,page,media', 'id', 'ASC', '1,2', NULL, '{\"spacing\":\"cozy\",\"last_updated\":true,\"show_footer\":false,\"item_numbers\":false,\"currentView\":\"table\",\"revisions_count\":false,\"table\":{\"last_updated\":true},\"tiles\":{\"subtitle_column\":\"title\"},\"subtitle_column\":\"title\",\"type_column\":\"title\",\"title_column\":\"title\",\"comments_count\":false}');
    
# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
	('app_albums', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
	('app_albums', 2, 1, 2, 0, 1, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_albums', '{{title}}', '/api/preview/?preview=true&id={{id}}&table=app_albums', 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
