# Create Table

CREATE TABLE `app_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT 'Active status - Only active rows will be displayed',
  `sort` int(10) unsigned NOT NULL,
  `form` varchar(50) DEFAULT 'contact' COMMENT 'Name of form',
  `url` varchar(150) DEFAULT NULL COMMENT 'URL from where form data was posted',
  `subject` varchar(250) DEFAULT NULL COMMENT 'Subject of form data - this is usually how messages are identified',
  `message` text COMMENT 'Form contents',
  `email` varchar(150) DEFAULT NULL COMMENT 'Email Address of user that posted message',
  `subscribe` tinyint(1) DEFAULT NULL COMMENT 'Subscription status - if user subscribed to emails',
  `verified` tinyint(1) DEFAULT NULL COMMENT 'Indicates if user has verified email address',
  `name` varchar(100) DEFAULT NULL COMMENT 'Name of user that posted form data',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_forms', 'email', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 5, 'Email Address of user that posted message', '{\"id\":\"text_input\",\"read_only\":1}'),
	('app_forms', 'form', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 'Name of form', '{\"id\":\"text_input\",\"read_only\":1}'),
	('app_forms', 'id', 'INT', 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL),
	('app_forms', 'message', 'TEXT', 'textarea', NULL, NULL, NULL, NULL, NULL, 0, 0, 9, 'Form contents', '{\"id\":\"textarea\",\"read_only\":1}'),
	('app_forms', 'name', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 4, 'Name of user that posted form data', '{\"id\":\"text_input\",\"read_only\":1}'),
	('app_forms', 'sort', 'INT', 'sort', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, NULL, NULL),
	('app_forms', 'status', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 1, 0, 2, 'Active status - Only active rows will be displayed', '{\"id\":\"status\"}'),
	('app_forms', 'subject', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 6, 'Subject of form data - this is usually how messages are identified', '{\"id\":\"text_input\",\"read_only\":1}'),
	('app_forms', 'subscribe', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 7, 'Subscription status - if user subscribed to emails', NULL),
	('app_forms', 'url', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 10, 'URL from where form data was posted', '{\"id\":\"text_input\",\"read_only\":1}'),
	('app_forms', 'verified', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 8, 'Indicates if user has verified email address', NULL);

# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
	(1, 'app_forms', NULL, 'form,name,email,subject,subscribe,verified', 'id', 'ASC', '1,2', NULL, '{\"spacing\":\"cozy\",\"show_footer\":false,\"item_numbers\":false,\"last_updated\":true,\"currentView\":\"table\",\"revisions_count\":false,\"table\":{\"show_footer\":false,\"item_numbers\":false,\"last_updated\":true},\"tiles\":{\"subtitle_column\":\"title\"},\"subtitle_column\":\"title\",\"type_column\":\"title\",\"title_column\":\"title\",\"comments_count\":false}');

# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
	('app_forms', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
	('app_forms', 2, 1, 2, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_forms', '{{form}} : {{email}}', '', 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);