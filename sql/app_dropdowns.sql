# Create Table

CREATE TABLE `app_dropdowns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(10) unsigned NOT NULL,
  `name` varchar(150) DEFAULT NULL COMMENT 'Dot syntax path or name - must be unique to each form dropdown input',
  `value` varchar(30) DEFAULT NULL COMMENT 'Value of dropdown - can be the same as the display text',
  `text` varchar(150) DEFAULT NULL COMMENT 'Text to display in the dropdown',
  `section_basic` varchar(1) DEFAULT NULL COMMENT 'Basic Information',
  PRIMARY KEY (`id`),
  UNIQUE KEY `NAME` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_dropdowns', 'id', 'INT', 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL),
	('app_dropdowns', 'name', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 4, 'Dot syntax path or name - must be unique to each form dropdown input, i.e. Dropdown Group', '{\"id\":\"text_input\"}'),
	('app_dropdowns', 'section_basic', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 'Basic Information', '{\"id\":\"section_break\",\"instructions\":\"<p>Collection of all the data for dropdowns used within the applications. All fields are usually required, but you can duplicate the values for the texts.</p>\",\"title\":\"Basic Information\"}'),
	('app_dropdowns', 'sort', 'INT', 'sort', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, NULL, NULL),
	('app_dropdowns', 'status', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 'Active status - Only active rows will be displayed', '{\"id\":\"status\"}'),
	('app_dropdowns', 'text', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 6, 'Text to display in the dropdown', NULL),
	('app_dropdowns', 'value', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 5, 'Value of dropdown - can be the same as the display text. Values must be unique within the same dropdown group.', '{\"id\":\"text_input\"}');

# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
	(1, 'app_dropdowns', NULL, 'name,value,text,sort', 'sort', 'ASC', '1,2', NULL, '{\"spacing\":\"cozy\",\"last_updated\":true,\"item_numbers\":false,\"currentView\":\"table\",\"show_footer\":false,\"revisions_count\":false,\"table\":{\"show_footer\":false,\"item_numbers\":false,\"last_updated\":true,\"comments_count\":false,\"revisions_count\":false},\"tiles\":{\"subtitle_column\":\"title\"},\"subtitle_column\":\"title\",\"type_column\":\"title\",\"title_column\":\"title\",\"comments_count\":true}');

# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
	('app_dropdowns', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
	('app_dropdowns', 2, 0, 0, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_dropdowns', '{{name}} - {{value}}', '/api/preview/?preview=true&table=app_dropdowns', 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);