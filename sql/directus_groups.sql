# Truncate table and insert

INSERT INTO `directus_groups` (`id`, `name`, `description`, `restrict_to_ip_whitelist`, `nav_override`)
VALUES
    (1, 'Administrator', 'Admins have access to all managed data within the system by default', NULL, NULL),
    (2, 'API', 'This sets the data that is publicly available through the API using a token', NULL, NULL),
    (3, 'Editor', 'Editors have access to edit table data and any system table allowed by Admins', NULL, NULL),
    (4, 'Public', 'This sets the data that is publicly available through the API without a token', NULL, NULL);