# Create Table

CREATE TABLE `app_previews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '2',
  `sort` int(10) unsigned NOT NULL,
  `title` varchar(50) DEFAULT NULL COMMENT 'Title of Page Preview',
  `description` varchar(140) DEFAULT NULL COMMENT 'Short description or call to action',
  `page` int(11) DEFAULT NULL COMMENT 'Page to which preview links to',
  `path` varchar(150) DEFAULT NULL COMMENT 'URL or Path to which preview links - overrides Page',
  `image` int(11) DEFAULT NULL COMMENT 'Preview Image',
  `section` varchar(20) DEFAULT NULL COMMENT 'Page preview section - useful if grouping previews',
  `section_basic` varchar(1) DEFAULT NULL COMMENT 'Basic Information',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_previews', 'description', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 5, 'Short description or call to action', NULL),
	('app_previews', 'id', 'INT', 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL),
	('app_previews', 'image', 'INT', 'single_file', 'MANYTOONE', 'directus_files', NULL, NULL, 'image', 0, 1, 6, 'Preview Image', NULL),
	('app_previews', 'page', 'INT', 'many_to_one', 'MANYTOONE', 'app_pages', NULL, NULL, 'page', 0, 0, 7, 'Page to which preview links to', '{\"id\":\"many_to_one\",\"visible_column\":\"name,headline\",\"visible_column_template\":\"{{name}}: {{headline}}\",\"allow_null\":1,\"filter_column\":\"name\"}'),
	('app_previews', 'path', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 8, 'URL or Path to which preview links - overrides Page', NULL),
	('app_previews', 'section', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 9, 'Page preview section - useful if grouping previews', NULL),
	('app_previews', 'section_basic', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 'Basic Information', '{\"id\":\"section_break\",\"instructions\":\"<p>Page previews or cards usually consisting of images with title, descriptions, and links to Pages within the application. Previews can be different from the information in App Pages, as they usually include a call to action.</p>\",\"title\":\"Basic Information\"}'),
	('app_previews', 'sort', 'INT', 'sort', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, NULL, NULL),
	('app_previews', 'status', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, NULL, NULL),
	('app_previews', 'title', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 4, 'Title of Page Preview', NULL);

# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
	(1, 'app_previews', NULL, 'title,description,page,path,image,section', 'sort', 'ASC', '1,2', NULL, NULL);

# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
	('app_previews', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
	('app_previews', 2, 1, 2, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_previews', '{{title}}: {{page}}', '/api/preview/?preview=true&id={{id}}&table=app_previews', 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
