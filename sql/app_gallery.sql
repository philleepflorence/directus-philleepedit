# Create Table

CREATE TABLE `app_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) NOT NULL DEFAULT '2',
  `sort` int(10) unsigned NOT NULL,
  `image` int(11) DEFAULT NULL COMMENT 'Gallery Image - Thumbnails are automatically created',
  `title` varchar(150) DEFAULT NULL COMMENT 'Title to display - Optional if text is embedded in image',
  `description` mediumtext COMMENT 'Description to display - Optional if text is embedded in image',
  `page` int(11) DEFAULT NULL COMMENT 'Select page to embed gallery images',
  `path` varchar(150) DEFAULT NULL COMMENT 'Internal URL Path - Do not enter http://www.domain.com/',
  `position` varchar(50) DEFAULT NULL COMMENT 'Position of Title and Description - For use in slideshows',
  `url` varchar(150) DEFAULT NULL COMMENT 'External URL: Will open in a new window/tab - For long URLs use https://bitly.com/ to shorten URL',
  `section_basic` varchar(1) DEFAULT NULL COMMENT 'Basic Information',
  PRIMARY KEY (`id`),
  KEY `UNIQUES` (`title`,`page`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_gallery', 'active', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 'Active status - Only active rows will be displayed', NULL),
	('app_gallery', 'description', 'TEXT', 'wysiwyg', NULL, NULL, NULL, NULL, NULL, 0, 0, 6, 'Description to display - Optional if text is embedded in image', NULL),
	('app_gallery', 'id', 'INT', 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL),
	('app_gallery', 'image', 'INT', 'single_file', 'MANYTOONE', 'directus_files', NULL, NULL, 'image', 0, 1, 4, 'Gallery Image - Thumbnails are automatically created', NULL),
	('app_gallery', 'page', 'INT', 'many_to_one', 'MANYTOONE', 'app_pages', NULL, NULL, 'page', 0, 0, 8, 'Select page to embed gallery images', '{\"id\":\"many_to_one\",\"readonly\":\"0\",\"visible_column\":\"name,path\",\"visible_column_template\":\"{{name}} - {{path}}\",\"placeholder_text\":\"Select Page\",\"filter_column\":\"name\",\"visible_status_ids\":\"1\",\"allow_null\":\"0\",\"filter_type\":\"dropdown\"}'),
	('app_gallery', 'path', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 9, 'Internal URL Path, when image is clicked - Do not enter http://www.domain.com/', '{\"id\":\"text_input\"}'),
	('app_gallery', 'position', 'VARCHAR', 'dropdown', NULL, NULL, NULL, NULL, NULL, 0, 0, 7, 'Position of Title and Description - For use in slideshows', '{\"id\":\"dropdown\",\"options\":\"{\\r\\n\\t\\\"\\\": \\\"None\\\",\\r\\n\\t\\\"bottom left\\\": \\\"Bottom Left\\\",\\r\\n\\t\\\"bottom right\\\": \\\"Bottom Right\\\",\\r\\n\\t\\\"aligncenter\\\": \\\"Center\\\",\\r\\n\\t\\\"alignrightcenter\\\": \\\"Center Right\\\",\\r\\n\\t\\\"alignleftcenter\\\": \\\"Center Left\\\"\\r\\n}\",\"placeholder\":\"Select Text Position\",\"read_only\":\"0\",\"use_native_input\":\"0\",\"list_view_formatting\":\"text\"}'),
	('app_gallery', 'section_basic', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 'Basic Information', '{\"id\":\"section_break\",\"instructions\":\"<p>Gallery or content images and slideshows. Can be included on one or more pages or sections.</p>\",\"title\":\"Basic Information\"}'),
	('app_gallery', 'sort', 'INT', 'sort', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, NULL, NULL),
	('app_gallery', 'title', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 5, 'Title to display - Optional if text is embedded in image', NULL),
	('app_gallery', 'url', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 10, 'External URL, when image is clicked: Will open in a new window/tab - For long URLs use https://bitly.com/ to shorten URL', '{\"id\":\"text_input\"}');
    
# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
	(1, 'app_gallery', NULL, 'image,title,description,page,path', 'sort', 'ASC', '1,2', NULL, '{\"spacing\":\"cozy\",\"show_footer\":false,\"item_numbers\":false,\"last_updated\":true,\"currentView\":\"table\",\"revisions_count\":false,\"table\":{\"last_updated\":true},\"tiles\":{\"subtitle_column\":\"path\"},\"subtitle_column\":\"title\",\"type_column\":\"title\",\"title_column\":\"title\"}');
    
# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
	('app_gallery', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
	('app_gallery', 2, 1, 0, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_gallery', '{{title}}', '/api/preview/?preview=true&id={{id}}&table=app_gallery', 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
