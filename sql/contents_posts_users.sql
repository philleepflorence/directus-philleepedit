# Create Table

CREATE TABLE `contents_posts_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) DEFAULT '1',
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `USER` (`user_id`),
  KEY `POST` (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
    ('contents_posts_users', 'id', NULL, 'numeric', NULL, NULL, NULL, NULL, NULL, 0, 1, 1, '', NULL),
    ('contents_posts_users', 'post_id', NULL, 'numeric', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 'ID or post', NULL),
    ('contents_posts_users', 'user_id', NULL, 'numeric', NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 'ID of user', NULL);

# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
    (1, 'contents_posts_users', NULL, 'user_id,post_id', 'id', 'ASC', '1,2', NULL, NULL);

# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
    ('contents_posts_users', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
    ('contents_posts_users', 2, 0, 0, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
    ('contents_posts_users', NULL, NULL, 1, 0, 1, 0, NULL, 'id', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);