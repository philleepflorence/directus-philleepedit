# Create Table

CREATE TABLE `app_labels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` int(10) unsigned NOT NULL,
  `section` varchar(100) DEFAULT NULL COMMENT 'Select section in app where to display label',
  `name` varchar(100) DEFAULT NULL COMMENT 'Name of Label',
  `value` text COMMENT 'Text to display',
  `show_html` tinyint(4) DEFAULT NULL COMMENT 'Display label as HTML or TEXT. If TEXT, HTML tags will be removed.',
  `page` int(11) DEFAULT NULL COMMENT 'Internal URL Path of Page - select from App Pages',
  `source` varchar(100) DEFAULT NULL COMMENT 'Path to source. E.g. listing.square_footage for Listings Square Footage',
  `url` varchar(150) DEFAULT NULL COMMENT 'Internal or External URL Path of Label - To use Page URL, enter true',
  `format` varchar(100) DEFAULT NULL COMMENT 'Additional format to apply to label - if applicable',
  `active` tinyint(1) DEFAULT '1',
  `section_basic` varchar(1) DEFAULT NULL COMMENT 'Basic Information',
  `section_options` varchar(1) DEFAULT NULL COMMENT 'Additional options',
  PRIMARY KEY (`id`),
  UNIQUE KEY `NAME` (`section`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_labels', 'active', 'TINYINT', 'status', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, '', NULL),
	('app_labels', 'format', 'VARCHAR', 'dropdown', NULL, NULL, NULL, NULL, NULL, 0, 0, 9, 'Additional format to apply to label - if applicable', '{\"id\":\"dropdown\",\"options\":\"{\\r\\n\\t\\\"\\\": \\\"None\\\",\\r\\n\\t\\\"date\\\": \\\"Date\\\",\\r\\n\\t\\\"datetime\\\": \\\"Date and Time\\\",\\r\\n\\t\\\"months\\\": \\\"Months\\\",\\r\\n\\t\\\"price\\\": \\\"Price\\\"\\r\\n}\",\"allow_null\":\"1\",\"list_value\":\"text\",\"placeholder_text\":\"Select format - optional\",\"select_multiple\":\"0\",\"display_search\":\"auto\",\"auto_search_limit\":\"10\",\"input_type\":\"dropdown\",\"delimiter\":\",\",\"read_only\":\"0\",\"placeholder\":\"\",\"use_native_input\":0,\"list_view_formatting\":\"text\"}'),
	('app_labels', 'id', 'INT', 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL),
	('app_labels', 'name', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 4, 'Name of Label - dot syntax format', '{\"id\":\"text_input\"}'),
	('app_labels', 'page', 'INT', 'many_to_one', 'MANYTOONE', 'app_pages', NULL, NULL, 'page', 0, 0, 11, 'Page in which label belongs', '{\"id\":\"many_to_one\",\"visible_column\":\"name,path\",\"visible_column_template\":\" {{path}} : {{name}}\",\"allow_null\":0,\"filter_column\":\"name\",\"placeholder_text\":\"Select App Page - Optional\",\"readonly\":\"0\",\"visible_status_ids\":\"1\",\"filter_type\":\"dropdown\",\"placeholder\":\"\"}'),
	('app_labels', 'section', 'VARCHAR', 'dropdown', NULL, NULL, NULL, NULL, NULL, 0, 1, 6, 'Select section in app where to display label', '{\"id\":\"dropdown\",\"options\":\"{\\n\\t\\\"buttons\\\": \\\"Buttons\\\",\\n\\t\\\"content\\\": \\\"Content\\\",\\n\\t\\\"footer\\\": \\\"Footer\\\",\\n\\t\\\"form\\\": \\\"Form\\\",\\n\\t\\\"header\\\": \\\"Header\\\",\\n\\t\\\"navigation\\\": \\\"Navigation\\\"\\t\\n}\",\"allow_null\":\"0\",\"select_multiple\":\"0\",\"display_search\":\"auto\",\"auto_search_limit\":\"10\",\"list_value\":\"text\",\"placeholder_text\":\"\",\"input_type\":\"dropdown\",\"delimiter\":\",\",\"use_native_input\":\"0\",\"read_only\":\"0\",\"placeholder\":\"\",\"list_view_formatting\":\"text\"}'),
	('app_labels', 'section_basic', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 'Basic Information', '{\"id\":\"section_break\",\"title\":\"Basic Information\",\"instructions\":\"<p>To avoid clashes, you can use dot syntax for the name. For example, contact.headline would be different from about.headline.</p>\"}'),
	('app_labels', 'section_options', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 7, 'Additional options', '{\"id\":\"section_break\",\"title\":\"Additional options\",\"instructions\":\"<p>These are all optional.</p>\"}'),
	('app_labels', 'show_html', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 8, 'Display label as HTML or TEXT. If TEXT, HTML tags will be removed. Some labels, such as buttons, will not display HTML!', '{\"id\":\"toggle\"}'),
	('app_labels', 'sort', 'INT', 'sort', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, NULL, NULL),
	('app_labels', 'source', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 10, 'Path to source, if getting from an external source or data object', '{\"id\":\"text_input\"}'),
	('app_labels', 'url', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 12, 'Internal or External URL Path of Label', '{\"id\":\"text_input\"}'),
	('app_labels', 'value', 'TEXT', 'wysiwyg', NULL, NULL, NULL, NULL, NULL, 0, 1, 5, 'Text to display', NULL);

# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
    (1, 'app_labels', NULL, 'page,section,name,value,show_html', 'name', 'ASC', '1,2', 'q%3Acta', '{\"spacing\":\"cozy\",\"last_updated\":true,\"show_footer\":false,\"item_numbers\":false,\"currentView\":\"table\",\"revisions_count\":false,\"table\":{\"last_updated\":true},\"tiles\":{\"subtitle_column\":\"title\"},\"subtitle_column\":\"title\",\"type_column\":\"title\",\"title_column\":\"title\",\"comments_count\":false}');

# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
    ('app_labels', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
    ('app_labels', 2, 1, 0, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_labels', '{{name}} - {{value}}', '/api/preview/?preview=true&id={{id}}&table=app_labels', 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
