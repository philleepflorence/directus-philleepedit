# Create Table

CREATE TABLE `app_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT 'Name of the template - use dot syntax if applicable',
  `template` text COMMENT 'Template - be sure to enter valid HTML only!',
  `layout` varchar(20) DEFAULT 'email' COMMENT 'Layout to use when rendering template - most templates use email layouts',
  `icon` varchar(50) DEFAULT NULL COMMENT 'Icon to use in template or layout - See App Icons for more information',
  `section_basic` varchar(1) DEFAULT NULL COMMENT 'Basic Information',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Insert Defaults into Table

INSERT INTO `app_templates` (`name`, `template`, `layout`, `icon`)
VALUES
    ('error.400', '<h2>Bad Request!</h2>\n<p><strong>We\'re sorry... but our infant application could not understand the request.</strong></p>\n<p>Are you sure you pressed the correct launch code? <br />The server could not understand the request due to invalid syntax. <br />Refreshing the page will be futile! <br />A few things you could do meanwhile: check for errors in the URL and clear your browser\'s cache for this application.</p>', 'plain', 'error'),
    ('error.401', '<h2>Unauthorized!</h2>\n<p><strong>We\'re sorry... you are not authorized to view this page.</strong></p>\n<p><strong>No Public Access!</strong> <br />The server understood the request but is refusing to fulfill it. <br />Authorization will help and the request should not be repeated.</p>\n<p><a title=\"Sign in to continue!\" href=\"../login\">Sign in to continue!</a></p>', 'plain', 'error'),
    ('error.403', '<h2>Forbidden!</h2>\n<p><strong>We\'re sorry...you are not authorized to view this page.</strong></p>\n<p>You can\'t go beyond this point... <br />The server understood the request but is refusing to fulfill it. <br />Authorization will not help and the request should not be repeated.</p>\n<p><a title=\"Let\'s get you home!\" href=\"../\">Let\'s get you home!</a></p>', 'plain', 'error'),
    ('error.404', '<h2>Page Not Found!</h2>\n<p><strong>You weren\'t really expecting a page now, were you?</strong></p>\n<p>Whoops looks like we lost one... <br />The server has not found anything matching the requested URI.</p>\n<p><a title=\"Let\'s get you somewhere safe!\" href=\"../\">Let\'s get you somewhere safe!</a></p>', 'plain', 'error'),
    ('error.500', '<h2>Internal Server Error!</h2>\n<p><strong>The server encountered an unexpected condition which prevented it from fulfilling the request.</strong></p>\n<p>There seems to be an internal error on the server... <br />A 500 error occurs when something blows up with our code. <br />You viewing this does not fix the code. <br />In fact, you\'re likely pissed off at the moment. <br />Take solace, we will work hard to fix this!</p>', 'plain', 'error'),
    ('error.503', '<h2>Service Unavailable!</h2>\n<p><strong>The server is currently unable to handle the request due to a temporary overloading or maintenance of the server.</strong></p>\n<p>Welcome to 503! <br />Something might be wrong with our server and we are looking into it and should have the problem sorted out in a jiffy.<br />Please see below for more details if applicable.</p>', 'plain', 'error');

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_templates', 'icon', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 'Icon to use in template or layout - See App Icons for more information', NULL),
	('app_templates', 'id', 'INT', 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL),
	('app_templates', 'layout', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 4, 'Layout to use when rendering template - most templates use email layouts', NULL),
	('app_templates', 'name', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 2, 'Name of the template - use dot syntax if applicable', NULL),
	('app_templates', 'section_basic', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 5, 'Basic Information', '{\"id\":\"section_break\",\"instructions\":\"<p>Templates used throughout the application, such as Email or Error Templates. To use dynamic data, simply do <em>{path.to.data}</em>, for example <em>{form.email}</em> will get the email from the form submitted. Consult with your webmaster on the data available to the template.</p>\",\"title\":\"Basic Information\"}'),
	('app_templates', 'template', 'TEXT', 'wysiwyg_full', NULL, NULL, NULL, NULL, NULL, 0, 1, 5, 'Template - be sure to enter valid HTML only!', NULL);

# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
    (1, 'app_templates', NULL, 'name,template,layout,icon', 'id', 'ASC', '1,2', NULL, '{\"spacing\":\"cozy\",\"show_footer\":false,\"item_numbers\":false,\"last_updated\":true,\"currentView\":\"table\",\"revisions_count\":false,\"table\":{\"show_footer\":false,\"item_numbers\":false,\"last_updated\":true},\"tiles\":{\"subtitle_column\":\"title\"},\"subtitle_column\":\"title\",\"type_column\":\"title\",\"title_column\":\"title\",\"comments_count\":false}');

# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
    ('app_templates', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
    ('app_templates', 2, 0, 0, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_templates', '', '/api/preview/?preview=true&id={{id}}&table=app_templates', 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);