# Create Table

CREATE TABLE `app_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `path` varchar(150) DEFAULT NULL COMMENT 'URL Slug and Permalink',
  `description` varchar(150) DEFAULT NULL,
  `tag_type` varchar(50) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '2',
  `parent` int(11) DEFAULT NULL,
  `section_basic` varchar(1) DEFAULT NULL COMMENT 'Basic Information',
  PRIMARY KEY (`id`),
  UNIQUE KEY `PATH` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('app_tags', 'active', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 'Active status - Only active rows will be displayed', NULL),
	('app_tags', 'description', NULL, 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 6, 'Short description of Tag or Category - Optional', NULL),
	('app_tags', 'id', NULL, 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, '', NULL),
	('app_tags', 'name', NULL, 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 3, 'Name of Tag or Category', NULL),
	('app_tags', 'parent', 'INT', 'many_to_one', 'MANYTOONE', 'app_tags', NULL, NULL, 'parent', 0, 0, 7, 'Tag Parent or Group - Optional', '{\"id\":\"many_to_one\",\"visible_status_ids\":\"1,2\",\"placeholder_text\":\"Select Parent Tag\",\"filter_column\":\"name\",\"visible_column_template\":\"{{name}} - {{path}} - {{tag_type}}\",\"visible_column\":\"name,path,tag_type\",\"readonly\":\"0\",\"allow_null\":\"0\",\"filter_type\":\"dropdown\"}'),
	('app_tags', 'path', 'VARCHAR', 'slug', NULL, NULL, NULL, NULL, NULL, 0, 1, 4, 'URL Slug and Permalink', '{\"id\":\"slug\",\"mirrored_field\":\"name\",\"readonly\":\"1\",\"size\":\"large\",\"only_on_creation\":\"0\"}'),
	('app_tags', 'section_basic', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 'Basic Information', '{\"id\":\"section_break\",\"instructions\":\"<p>Categories and tags of the application. For use with Posts, Events, and more.</p>\",\"title\":\"Basic Information\"}'),
	('app_tags', 'tag_type', NULL, 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 5, 'Type of Tag', NULL);

# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
    (1, 'app_tags', NULL, 'name,path,tag_type', 'id', 'ASC', '1', '', '{\"spacing\":\"cozy\",\"last_updated\":true,\"show_footer\":false,\"item_numbers\":false,\"currentView\":\"table\",\"revisions_count\":false,\"table\":{\"show_footer\":false,\"item_numbers\":false,\"last_updated\":true},\"tiles\":{\"subtitle_column\":\"title\"},\"subtitle_column\":\"title\",\"type_column\":\"title\",\"title_column\":\"title\"}');

# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
    ('app_tags', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
    ('app_tags', 2, 0, 0, 0, 0, 2, NULL, NULL, 1, NULL);

# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('app_tags', '{{name}} - ({{tag_type}})', '/api/preview/?preview=true&table=app_tags', 0, 0, 1, 0, NULL, 'id', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);