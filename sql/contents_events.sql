# Create Table

CREATE TABLE `contents_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL DEFAULT '2',
  `sort` int(10) unsigned NOT NULL,
  `section_break_basic_info` varchar(1) DEFAULT NULL COMMENT 'This info will also appear in Feeds and any ads created for this event',
  `name` varchar(150) DEFAULT NULL COMMENT 'Add a short, clear name of event',
  `image` int(11) DEFAULT NULL COMMENT 'Event flyer or image',
  `description` text COMMENT 'Tell people more about the event',
  `location` varchar(250) DEFAULT NULL COMMENT 'Include a place or address. A specific location helps guests know where to go. If empty, location in configuration will be used.',
  `section_break_details` varchar(1) DEFAULT NULL COMMENT 'Let people know what type of event you''re hosting and what to expect',
  `section_break_tickets` varchar(1) DEFAULT NULL COMMENT 'Let people know where they can get tickets for your event',
  `ticket_url` varchar(250) DEFAULT NULL COMMENT 'Add a link to your ticketing website. Add a link to a website where people can purchase tickets. For very long URLs, please use a URL Shortener such as https://goo.gl',
  `start_date` date DEFAULT NULL COMMENT 'When the event starts - leave blank for recurring events',
  `end_date` date DEFAULT '3000-01-01' COMMENT 'When the event ends - leave blank for recurring events',
  `start_time` time DEFAULT NULL COMMENT 'When event actually starts - required even if recurring event',
  `end_time` time DEFAULT NULL COMMENT 'When event actually starts - required even if recurring event',
  `recurring` tinyint(1) DEFAULT NULL COMMENT 'Recurring Event - Leave start and dates blank if event is ongoing and never ends',
  `featured` tinyint(1) DEFAULT NULL COMMENT 'Feature event in various sections of application',
  `occurence` varchar(100) DEFAULT NULL COMMENT 'Useful for displaying recurring events',
  `address` text COMMENT 'This is the address that will be displayed, you may format this address as you wish it displayed.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# Update Directus Columns

INSERT INTO `directus_columns` (`table_name`, `column_name`, `data_type`, `ui`, `relationship_type`, `related_table`, `junction_table`, `junction_key_left`, `junction_key_right`, `hidden_input`, `required`, `sort`, `comment`, `options`)
VALUES
	('contents_events', 'address', 'TEXT', 'wysiwyg', NULL, NULL, NULL, NULL, NULL, 0, 0, 6, 'This is the address that will be displayed, you may format this address as you wish it displayed.', '{\"id\":\"wysiwyg\",\"simple_editor\":1}'),
	('contents_events', 'category', 'ALIAS', 'many_to_many', 'MANYTOMANY', 'app_tags', 'contents_events_categories', 'event_id', 'tag_id', 0, 1, 7, 'Enter at least on tag as the category for the event - events will be groupedby categories', '{\"id\":\"many_to_many\",\"no_duplicates\":1,\"min_entries\":\"1\",\"visible_columns\":\"name,path,tag_type\",\"visible_column_template\":\"{{name}} ({{tag_type}})\",\"filter_column\":\"name\"}'),
	('contents_events', 'description', 'TEXT', 'wysiwyg_full', NULL, NULL, NULL, NULL, NULL, 0, 1, 18, 'Tell people more about the event. Make descriptions easy to read by using standard capitalization and common characters.', '{\"id\":\"wysiwyg_full\"}'),
	('contents_events', 'end_date', 'DATE', 'date', NULL, NULL, NULL, NULL, NULL, 0, 0, 15, 'When the event ends - leave default for recurring events', '{\"id\":\"date\",\"contextual_date_in_listview\":1,\"auto-populate_when_hidden_and_null\":0}'),
	('contents_events', 'end_time', 'TIME', 'time', NULL, NULL, NULL, NULL, NULL, 0, 1, 16, 'When event actually starts - required even if recurring event', NULL),
	('contents_events', 'featured', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 4, 'Feature event in various sections of application', '{\"id\":\"toggle\"}'),
	('contents_events', 'id', 'INT', 'primary_key', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, NULL, NULL),
	('contents_events', 'image', 'INT', 'single_file', 'MANYTOONE', 'directus_files', NULL, NULL, 'image', 0, 1, 9, 'Event flyer or image. For best results, choose a photo or video with an aspect ratio of 16:9 or landscape image.', '{\"id\":\"single_file\"}'),
	('contents_events', 'location', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 13, 'Include a place or address. A specific location helps guests know where to go. If empty, location in configuration will be used.', '{\"id\":\"text_input\"}'),
	('contents_events', 'name', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 1, 5, 'Add a short, clear name of event', '{\"id\":\"text_input\",\"placeholder\":\"\"}'),
	('contents_events', 'occurence', 'VARCHAR', 'dropdown', NULL, NULL, NULL, NULL, NULL, 0, 1, 11, 'Useful for displaying recurring events', '{\"id\":\"dropdown\",\"options\":\"{\\n\\t\\\"once\\\": \\\"Occurs once only\\\",\\n\\t\\\"daily\\\": \\\"Daily - Everyday\\\",\\n\\t\\\"weekdays\\\": \\\"Weekdays - Monday through Friday\\\",\\n\\t\\\"weekly\\\": \\\"Weekly - Once a week\\\",\\n\\t\\\"biweekly\\\": \\\"Bi-Weekly - Two times a month\\\",\\n\\t\\\"monthly\\\": \\\"Monthly - Once a month\\\",\\n\\t\\\"quarterly\\\": \\\"Quarterly - Four times a year\\\",\\n\\t\\\"yearly\\\": \\\"Yearly - Once a year\\\"\\n}\",\"placeholder\":\"\"}'),
	('contents_events', 'recurring', 'TINYINT', 'toggle', NULL, NULL, NULL, NULL, NULL, 0, 0, 10, 'Recurring Event - Leave start and dates blank if event is ongoing and never ends', '{\"id\":\"toggle\",\"label\":\"Ongoing Event\"}'),
	('contents_events', 'section_break_basic_info', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 2, 'This info will also appear in Feeds and any ads created for this event', '{\"id\":\"section_break\",\"title\":\"Basic Information\",\"instructions\":\"<p>This info will also appear in News Feed and any ads created for this event</p>\",\"show_inline\":0}'),
	('contents_events', 'section_break_details', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 17, 'Let people know what type of event you\'re hosting and what to expect', '{\"id\":\"section_break\",\"show_inline\":0,\"title\":\"Details\",\"instructions\":\"<p>Let people know what type of event you\'re hosting and what to expect</p>\"}'),
	('contents_events', 'section_break_tickets', 'VARCHAR', 'section_break', NULL, NULL, NULL, NULL, NULL, 0, 0, 19, 'Let people know where they can get tickets for your event', '{\"id\":\"section_break\",\"instructions\":\"<p>Any gathering, performance, occasion or celebration. Events can be recurring or can occur once. For news, use <em>Contents Posts</em> and select <em>News</em> as the Post Type. <br>Let people know where they can get tickets for your event</p>\",\"title\":\"Tickets\",\"show_inline\":0}'),
	('contents_events', 'sort', 'INT', 'sort', NULL, NULL, NULL, NULL, NULL, 1, 0, 1, NULL, NULL),
	('contents_events', 'start_date', 'DATE', 'date', NULL, NULL, NULL, NULL, NULL, 0, 1, 12, 'The date event starts or started', '{\"id\":\"date\",\"contextual_date_in_listview\":1,\"auto-populate_when_hidden_and_null\":0}'),
	('contents_events', 'start_time', 'TIME', 'time', NULL, NULL, NULL, NULL, NULL, 0, 1, 14, 'When event actually starts - required even if recurring event', '{\"id\":\"time\"}'),
	('contents_events', 'status', 'INT', 'status', NULL, NULL, NULL, NULL, NULL, 0, 0, 3, 'Active status - Only active rows will be displayed', NULL),
	('contents_events', 'tags', 'ALIAS', 'many_to_many', 'MANYTOMANY', 'app_tags', 'contents_events_tags', 'event_id', 'tag_id', 0, 0, 8, 'Add keywords that describe your event so it can be better recommended to people who are interested in that topic.', '{\"id\":\"many_to_many\",\"no_duplicates\":1,\"min_entries\":\"1\",\"filter_column\":\"name\",\"visible_columns\":\"name,path,tag_type\",\"visible_column_template\":\"{{name}} ({{tag_type}})\"}'),
	('contents_events', 'ticket_url', 'VARCHAR', 'text_input', NULL, NULL, NULL, NULL, NULL, 0, 0, 20, 'Add a link to your ticketing website. Add a link to a website where people can purchase tickets. For very long URLs, please use a URL Shortener such as https://goo.gl', NULL);

# Update directus Preferences

INSERT INTO `directus_preferences` (`user`, `table_name`, `title`, `columns_visible`, `sort`, `sort_order`, `status`, `search_string`, `list_view_options`)
VALUES
	(1, 'contents_events', NULL, 'name,image,occurence,start_date,start_time,end_date,end_time', 'id', 'ASC', '1,2', NULL, '{\"spacing\":\"cozy\",\"last_updated\":true,\"show_footer\":false,\"item_numbers\":false,\"currentView\":\"table\",\"revisions_count\":false,\"table\":{\"show_footer\":false,\"item_numbers\":false,\"last_updated\":true},\"tiles\":{\"subtitle_column\":\"title\"},\"subtitle_column\":\"title\",\"type_column\":\"title\",\"title_column\":\"title\"}');


# Update Directus Privileges

INSERT INTO `directus_privileges` (`table_name`, `allow_view`, `allow_add`, `allow_edit`, `allow_delete`, `allow_alter`, `group_id`, `read_field_blacklist`, `write_field_blacklist`, `nav_listed`, `status_id`)
VALUES
	('contents_events', 2, 1, 2, 2, 1, 1, NULL, NULL, 1, NULL),
	('contents_events', 2, 0, 0, 0, 0, 2, NULL, NULL, 1, NULL);


# Update Directus Tables

INSERT INTO `directus_tables` (`table_name`, `display_template`, `preview_url`, `hidden`, `single`, `default_status`, `footer`, `column_groupings`, `primary_column`, `sort_column`, `status_column`, `status_mapping`, `user_create_column`, `user_update_column`, `date_create_column`, `date_update_column`, `filter_column_blacklist`)
VALUES
	('contents_events', '', '/api/preview/?preview=true&id={{id}}&table=contents_events', 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);