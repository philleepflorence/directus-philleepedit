var APP = function ()
{
	var canceltimers, formtimer, tabletimer, pagetimer, uitimer, activeNav, Router, router, user, version = '1.1';
	
	/*
		Helper Methods!
	*/
	
	this.tables = function ()
	{
		/*
			Clone and filter window.directusData.tables to avoid breaking original array
			Do not allow CSV uploads to directus system tables - server validation also implemented!
		*/
		
		var tables = window.directusData.tables.slice(0);
		
		tables = tables.filter(function (row, index)
		{
			row.schema.display_name = row.schema.name.replace(/_/g, ' ').replace(/\b\w/g, function (input)
			{
				return input.toUpperCase();
			});
			
			return row.schema.name.indexOf('directus_') < 0;
		});
		
		return tables;
	};
	
	/*
		HandleBars Compile
		ARGUMENTS:
			data - @Object
			template - @String: name of template - see templates
	*/
	
	var compile = function (data, template)
	{
		var src = templates[template];

		if (!src) return '';

		var compiled = Handlebars.compile(src);
		var result = compiled(data);

		return result;
	};
	
	/*
		Convert CSV File into JSON Object or string
		ARGUMENTS:
			csv - @string - CSV file with headers
			json - @boolean - Return JavaScript object if null, Pretty JSON if true
			skipempty - @boolean - Skip empty values - returns JSON without empty or null values
	*/
	
	var csv2json = function (csv, json, skipempty)
	{
		var lines = csv.split("\n");
		var result = [];
		var headers = lines[0].split(",");
		
		var isEmpty = function (obj)
		{
			for(var prop in obj) if(obj.hasOwnProperty(prop)) return false;
			
			return JSON.stringify(obj) === JSON.stringify({});
		};
		
		for (var i=1; i < lines.length; i++)
		{
			var obj = {};
			var currentline = lines[i].split(",");
			
			for (var j=0; j < headers.length; j++) 
			{
				/*
					Strip quotes from headers and process values
				*/
				
				var header = headers[j].replace(/[^a-z0-9_]/g, '');
				var value = currentline[j];
				
				if (typeof value === 'string') value = value.trim();
				
				/*
					Ignore empty strings or values
				*/
				
				if (skipempty && ( !value || !value.length || value.toUpperCase() == 'NULL' || value.toUpperCase().indexOf('NULL') >= 0 )) continue;
				
				if (/^\d+$/.test(value)) value = Number(value);
				
				/*
					Strip quotes from values if to prevent double quotes
				*/
				
				if (typeof value === 'string' && value && value.charAt(0) === '"') value = value.substr(1);
				
				if (typeof value === 'string' && value && value.charAt(value.length - 1) === '"') value = value.substr(0, value.length - 1);
				
				obj[header] = value;
			}
			
			if (skipempty && isEmpty(obj)) continue;
			
			result.push(obj);
		}
		
		if (!json) return result;
		
		return JSON.stringify(result, null, 4);
	};
	
	/*
		Syntax Highlighter
	*/
	
	this.syntax = {
		json: function (json, hightlight)
		{
			/*
				Convert to string!
			*/
			
			if (typeof json === 'object') json = JSON.stringify(json, undefined, 4);
			
			json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
						
			return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,  function (match)
			{
				var cls = 'number';
				
				if (/^"/.test(match))
				{
					if (/:$/.test(match)) cls = 'key';
					else cls = 'string';
				}
				else if (/true|false/.test(match)) cls = 'boolean';
				else if (/null/.test(match)) cls = 'null';
				
				return '<span class="' + cls + '">' + match + '</span>';
			});
		}
	};
	
	/*
		Create Query String from JSON formatted object
		Users recursive looping for depths greater than 1
	*/
	
	var json2query = function (obj, recursive, encode, prefix)
	{
		if (!obj) return obj;
		
		var str = [], p;
		
		if (!recursive) 
		{
			for (var p in obj) 
			{
				if (obj.hasOwnProperty(p)) str.push(( encode ? encodeURIComponent(p) : p ) + "=" + ( encode ? encodeURIComponent(obj[p]) : obj[p] ));
			}
		}
		else if (recursive)
		{
			for (p in obj)
			{
				if (obj.hasOwnProperty(p))
				{
					var k = prefix ? prefix + "[" + p + "]" : p, v = obj[p];
					
					str.push((v !== null && typeof v === "object") ? serialize(v, recursive, encode, k) : ( encode ? encodeURIComponent(k) : k ) + "=" + ( encode ? encodeURIComponent(v) : v ));
				}
			}
		}
		
		return str.join("&");
	};
	
	/*
		Form helper for all methods using the editor table rows
		REQUIREMENTS:
			Use of the <data-app-templates="editor-tables-form"> Template
	*/
	
	var form = {
		columns: function ()
		{
			$('input[data-editor-table-column]').each(function (index, input)
			{
				var column = input.getAttribute('data-editor-table-column');
				var $columns = $('[data-editor-column=":column"]'.replace(':column', column));
				
				if ($(input).is(":checked")) $columns.removeClass('hidden');
				else $columns.addClass('hidden');
			});
		},
		handlers: function ()
		{
			$('[data-editor-input]').off('change').on('change', function (e)
			{
				$(this).parents('tr').addClass('active');
				
				$(this).parents('td').add(this).addClass('active'); 
			});
			
			$('input[data-search-input]').off('change').on('change', function (e)
			{
				form.search(this.value);
			});
			
			$('input[data-editor-table-column]').off('change').on('change', function ()
			{
				form.columns();
			});
		},	
		name: function (input)
		{
			if (!input) return input;
			
			return input.replace(/_/g, ' ').replace(/\b\w/g, function (input)
			{
				return input.toUpperCase();
			})
		},		
		process: function ()
		{
			/*
				Process edited rows only
			*/
			
			$rows = $('[data-editor-row].active');
			
			if (!$rows.get(0)) return false;
			
			var processed = [];
			
			$rows.each(function (index, row)
			{
				var curr = {
					id: Number( row.getAttribute('data-editor-row') )
				};
				
				$(row).find('[data-editor-column].active').each(function (key, element)
				{
					var input = $(element).find('[data-editor-input].active').get(0);
					var key = element.getAttribute('data-editor-column');
					
					$(input).removeClass('error');
					
					/*
						Process validation if applicable
					*/
					
					var regex = input.getAttribute('data-validate');
					var message = input.getAttribute('data-validate-message') || templates.form.error;
					
					regex = regex ? new RegExp(regex) : regex;
					
					if (input.value && input.value.length && regex && message && !regex.test(input.value)) 
					{
						$(input).addClass('error');
						
						message += ' Row: ' + Number(index + 1) + ' - ' + form.name(key).toUpperCase();
						
						return app.alert(message, 'error');
					}
					
					curr[key] = input.value;
				});
				
				processed.push(curr);
			});
			
			return processed;
		},
		search: function (value)
		{
			if (!value) return $('[data-editor-row]').removeClass('hidden');
			
			value = value.toLowerCase();
			
			$('[data-editor-row]').each(function (index, row)
			{
				var text = '';
				
				$(row).find('[data-editor-input]').each(function (key, input)
				{
					text = text + ' ' + input.value;
				});
				
				if (text.toLowerCase().indexOf(value) >= 0) $(row).removeClass('hidden');
				else $(row).addClass('hidden');
			});			
		},
		submit: function (table, rows, done)
		{
			$.post('/api/rows', 
			{
				rows: rows,
				table: table
			}, 
			function (data, response)
			{
				data = data || {};
				
				if (data.error) return app.alert(data.error, 'error');
				
				done(data, response);
				
			}, 'json');
		}
	};

	var templates = {
		cdn: {
			error: "Something went wrong! Unable to load CDN information. <br>Please try again or contact the webmaster!",
			loading: "Please wait, scanning CDN ...",
			loaded: "CDN Assets loaded!",
			resizing: "Please wait, creating sizes, it make take up to 5 minutes to resize new images ...",
			resized: "Number of images available for resizing: :images. <br>Number of image sizes created: :len."
		},
		csv: {
			error: "Something went wrong! Unable to load tables. <br>Please try again or contact the webmaster!",
			success: "Bulk table updates successful. <br>Number of rows updated: :rows!",
			file: {
				error: "Something went wrong! Unable to process CSV. <br>Please try again or contact the webmaster!",
				verify: "Please verify the Preview before uploading! <br>Any invalid fields or values will causes the server to return an error!",
				json: "Please select CSV file before attempting to upload!"
			}
		},
		editor: {
			options: "No tables or options have been configured yet! <br>Please try again or contact the webmaster!",
			error: "Something went wrong! Unable to load tables. <br>Please try again or contact the webmaster!",
			loading: "Please wait, loading table rows ...",
			rows: "There are no rows available for :table!",
			submit: {
				error: "Something went wrong! Unable to process bulk updates. <br>Please try again or contact the webmaster!",
				table: "Something went wrong! Unable to process table. <br>Please try again or contact the webmaster!",
				rows: "You have not edited any rows yet! <br>Only edited rows are processed.",
				success: "Bulk table updates successful. Number of rows updated: :rows!"
			}
		},
		form: {
			error: "One or more inputs failed validation. Please enter valid values and try again!"
		},
		footer: {
			description: '<footer class=\"description animated fadeIn\"><h3 class=\"description\" data-overlay-content=\"/philleepedit/app/templates/guide.html\">:html<i class=\"material-icons center\">info</i></h3></footer>',
			pages: 'Large content blocks for the :page page. For the :page page metadata, see App Pages. For headlines and forms, see App Labels.'
		},
		previews: {
			error: "Something went wrong! Unable to load tables. <br>Please try again or contact the webmaster!",
			processing: "Please wait... Processing and formatting JSON!"
		},
		uploads: {
			error: {
				album: "Unable to create album. <br>Something went wrong please contact the webmaster!",
				apikey: "You must have a valid access token to perform uploads! <br>Please log out and log in and try again!",
				form: "Missing library or upload form! <br>Please try again or contact the webmaster!",
				nofile: "Nothing to upload! <br>Please try again or contact the webmaster!"
			},
			status: {
				album: "Creating Album: :title ... Please wait ...",
				processing: "Processing and uploading image :curr of :length ... <br>Please wait ..."
			},
			success: {
				album: "Album: :title, was successfully created!",
				images: "Images successfully uploaded: :length!"
			}
		}
	};

	var navigation = function ()
	{
		if ($('#mainSidebar .scroll-y a li.active').length) return false;

		var path = window.location.pathname;
			path = path.indexOf('/') === 0 ? path.slice(1) : path;

		$('#mainSidebar .scroll-y a li').removeClass('active');

		$('#mainSidebar .scroll-y a').each(function ()
		{
			var href = $(this).attr('href');
				href = href.indexOf('/') === 0 ? href.slice(1): href;

			if (href === path) $(this).find('li').addClass('active');
		});
	};

	var overlay = function (e)
	{
		var element = e.currentTarget;

		var path = element.getAttribute('data-overlay-content');

		var $content = $('#content');

		if (!path || !$content.get(0)) return;

		$content.find('.app-overlay').remove();

		$.get(path, function (template)
		{
			/*
				Replace dynamic variables
			*/

			template = template.replace('{{domain}}', window.location.hostname);

			$content.prepend(template);
			
			/*
				Hide all non installed table helpers
			*/
			
			var tables = app.tables();
			var $tables = $('.app-overlay').find('[data-overlay-table]');
			
			$tables.addClass('hidden');
			
			tables.forEach(function (row, index)
			{
				$tables.filter('[data-overlay-table=":table"]'.replace(':table', row.schema.name)).removeClass('hidden');
			});
			
			/*
				Event Handlers
			*/

			$('[data-overlay-close]').on('click', function (e)
			{
				e.preventDefault();

				$('#mainSidebar .scroll-y a li').removeClass('active');

				if (activeNav) activeNav.addClass('active');

				$(this).parents('.app-overlay').remove();
			});

			$('[data-overlay-section]').on('click', function (e)
			{
				var target = $(this).attr('data-overlay-section');

				$('[data-overlay-section]').removeClass('active');

				$(this).addClass('active');

				if (!target)
				{
					$('[data-overlay-section]').removeClass('active');

					$('.app-overlay-section').removeClass('hidden');
				}
				else
				{
					$('.app-overlay-section').addClass('hidden');

					$(target).removeClass('hidden');
				}
			});
		});
	};
	
	this.previews = function ()
	{
		if (!window.directusData.tables) return app.alert(templates.previews.error, 'error');
		
		var tables = app.tables();
		
		/*
			Compile Tables and Fields
		*/
		
		var html = compile(tables, 'previews-tables-fields');
		
		$('[app-template-cdn="previews-tables-fields"]').html(html);
		
		/*
			Event Handlers
		*/
		
		$('[data-previews-anchor]').on('click', function (e)
		{
			var url = this.getAttribute('data-previews-url');
			
			$('[data-previews-anchor]').removeClass('active');
			
			$(this).addClass('active');
			
			/*
				Get table rows
			*/
			
			url += '/rows';
			
			return load(url);
		});
		
		var load = function (url)
		{
			$.getJSON(url, function (data)
			{
				render(data);
			});
		};
		
		var render = function (data)
		{
			if (!data || !data.data) return app.alert(templates.previews.error, 'error'); 
			
			var string = app.syntax.json(data);
			
			string = '<pre>:string</pre>'.replace(':string', string);
			
			$('[data-previews-json]').html(string);
		};
	};

	var ui = function ()
	{
		if (!app.data) return false;

		var reset = function ()
		{
			$('#mainSidebar .scroll-y').addClass('animated fadeIn');
		};

		/*
			Add descriptions to pages as defined in the storage/app/data/app.json pages property
		*/

		var html, page = app.data && app.data.pages ? app.data.pages.filter(function (row)
		{
			return row.pathname == window.location.pathname;
		}) : null;

		page = page ? page[0] : page;
		
		if (page) html = page.description;				
		else if (window.location.pathname.indexOf('/tables/pages_') === 0) html = templates.footer.pages.replace(/:page/g, ( window.location.pathname.replace('/tables/pages_', '').replace(/_/g, '') ));		

		if (html)
		{
			clearInterval(pagetimer);

			pagetimer = setInterval(function ()
			{
				if ($('#page-content').get(0))
				{
					clearInterval(pagetimer);

					if (!$('#page-content h3.description').get(0) && html)
					{
						$('#page-content').append(templates.footer.description.replace(':html', html));
					}
				}
			}, 500);
		}

		/*
			Clear all timers after a certain time to prevent endless loop
		*/

		clearTimeout(canceltimers);

		canceltimers = setTimeout(function ()
		{
			clearInterval(pagetimer);

			clearInterval(formtimer);

			clearInterval(uitimer);

			reset();
		},
		20000);

		/*
		    UI elements - Update UI
		*/

		$('#mainSidebar').removeAttr('data-ui-updated');

        clearInterval(uitimer);

		uitimer = setInterval(function ()
        {
			if ($('#mainSidebar').get(0))
            {
                clearInterval(uitimer);

				/*
				    Add dividers to navigation elements
				*/

                if (!$('#mainSidebar').attr('data-ui-updated'))
        		{
                    $('#mainSidebar').attr('data-ui-updated', true);

					var $last = $('#mainSidebar a:contains("Pages ")').get(0) ? $('#mainSidebar a:contains("Pages ")') : $('#mainSidebar a:contains("Contents ")');

					$('#mainSidebar h3').first().text('Application Modules');

                    $('#mainSidebar a:contains("App ")').last().after('<div class="divider animated fadeIn"></div><h3>Application Contents</h3>');

					$('#mainSidebar a:contains("Pages ")').first().before('<div class="divider animated fadeIn"></div><h3>Application Pages</h3>');

                    $last.last().after(templates.menu);

					$('#mainSidebar .scroll-y').addClass('animated fadeIn');

					navigation();

					$('[data-overlay-content]').off('click').on('click', function (e)
					{
						e.preventDefault();

						activeNav = $('#mainSidebar .scroll-y a li.active');

						if (activeNav.parents('a').attr('data-overlay-content')) activeNav = null;

						$('#mainSidebar .scroll-y a li').removeClass('active');

						$(this).find('li').addClass('active');

						overlay(e);
					});
        		}
            }
            
            if (app.data.descriptions.pathname.indexOf(window.location.pathname) >= 0)
            {
	            $('tr[data-id]').each(function (index, element) 
	            {
		            var id = element.getAttribute('data-id');
		            
		            if (!id || element.getAttribute('data-title-rendered')) return false;
		            
		            element.setAttribute('data-title-rendered', true);
		            
		            if (id.indexOf('pages_') === 0) app.data.descriptions.tables[id] = app.data.descriptions.pages.replace(':page', ( id.replace('pages_', '').replace(/_/g, '') ));
		            
		            if (app.data.descriptions.tables[id]) $($(element).find('td').get(0)).append('<small data-table-description>:html</small>'.replace(':html', app.data.descriptions.tables[id]));		            
	            });
            }

        }, 500);

		clearInterval(formtimer);

		formtimer = setInterval(function ()
        {
            if ($('#page-content').get(0))
            {
                clearInterval(formtimer);

				/*
					Form UI - Make text fields expand to fit content
				*/

				var textfields = $('#page-content').find('.fields .field .interface textarea[readonly]');

				textfields.each(function (index, element)
				{
					if (element.scrollHeight > $(element).height()) $(element).css({ minHeight : element.scrollHeight, resize: 'none' });
				});
            }

        }, 500);
	};

	this.alert = function (message, type, options)
	{
		if (!message) return $(".noty_bar").trigger('click');

		var types = {
			alert : {
				type: 'alert',
				timeout: 3000
			},
			info : {
				type: 'info',
				timeout: 3000
			},
			success : {
				type: 'success',
				timeout: 15000
			},
			error : {
				type: 'error',
				timeout: 120000
			},
			warning : {
				type: 'warning',
				timeout: 30000
			}
		};

		type = types[type] || types.alert;
		options = options || {};

		var n = new noty({
			animation: {
				open : 'animated fadeInUp',
				close: 'animated fadeOutUp'
			},
			layout: 'bottomRight',
			text: message,
			timeout: options.timeout || type.timeout,
			type: type.type
		}).show();

		return false;
	};

	this.cdn = function ()
	{
		var details = function (e)
		{
			var element = e.currentTarget;
			var object = element.getAttribute('data-cdn-media-object');
			var id = element.getAttribute('data-cdn-media-id');
			var data = app.cdn.data[object]['data'][id];

			$('[data-cdn-media-details]').remove();

			if (!data) return false;

			/*
				Get last element in row
			*/

			var top = $(element).position().top;
			var next = $(element);

			$(element).nextAll('[data-cdn-media-object]').each(function ()
			{
				if (top < $(this).position().top)
				{
					next = $(this).prev('[data-cdn-media-object]');

					return false;
				}
			});

			/*
				Render template
			*/

			var html = compile(data, 'cdn-media-details');

			$(html).insertAfter(next);

			$('[data-cdn-media-details-close]').one('click', function ()
			{
				$(this).parents('[data-cdn-media-details]').remove();
			});
		};

		var load = function ()
		{
			app.alert(templates.cdn.loading);

			$.getJSON("/api/cdn", function (data)
			{
				app.cdn.data = data;

				render();
			});
		};

		var render = function ()
		{
			if (!app.cdn.data) return app.alert(templates.cdn.error, 'error');

			for (var i in app.cdn.data)
			{
				var row = app.cdn.data[i];

				if (row.data && row.template)
				{
					var content = $('[app-template-cdn=":template"]'.replace(':template', row.template));
					var html = compile(row.data, row.template);

					content.html(html);
				}
			}

			$('body').on('click', '[data-cdn-media-object]', function (e)
			{
				details(e);
			});

			$('[data-cdn-resize]').on('click', function (e)
			{
				resize(e);
			});

			app.alert(templates.cdn.loaded, "success");
		};

		var resize = function (e)
		{
			if (!app.cdn.data) return app.alert(templates.cdn.error, 'error');

			if (app.cdn.loading) return app.alert(templates.cdn.resizing, 'warning');

			app.cdn.loading = true;

			app.alert(templates.cdn.resizing);

			$.getJSON(app.cdn.data.resize.url, function (data)
			{
				app.alert(templates.cdn.resized.replace(':images', data.meta.images).replace(':len', data.meta.resized), 'success');

				app.cdn.loading = null;
			});
		};

		load();
	};
	
	this.csv = function ()
	{
		if (!window.directusData.tables || !window.File || !window.FileReader || !window.FileList || !window.Blob) return app.alert(templates.csv.error, 'error');
		
		var json;
		
		var form = function (element)
		{
			var columns = [];
			
			$(element).find('[data-column]').each(function (index, span)
			{
				var primary = span.getAttribute('data-primary-key') === 'PRI';
				var unique = span.getAttribute('data-primary-key') === 'UNI';
				var required = span.getAttribute('data-required') ? 'REQUIRED' : 'OPTIONAL';
				
				if (primary) required = required + ': UPDATE ONLY';
				
				if (unique) required = required + ': UNIQUE - NO DUPLICATES ALLOWED';
				
				var value = [span.getAttribute('data-column'), span.getAttribute('data-type'), required];
				
				columns.push(value.join(' - '));
			});
			
			$('.app-overlay-section').addClass('hidden');

			$('[data-csv-form-container]').removeClass('hidden');
			
			$('[data-csv-table]').val(element.getAttribute('data-csv-table'));
			$('[data-csv-columns]').val(columns.join('\n'));
			
			$('[data-csv-columns]').height( $('[data-csv-columns]').get(0).scrollHeight );
			
			return false
		};
		
		var process = function ()
		{
			var input = $('[data-csv-file]').get(0);
			var files = input.files;
			
			if (!input || !files[0]) return app.alert(templates.csv.file.error, 'error');
			
			var file = files[0];
			
			var filereader = new FileReader();
			
			filereader.onload = function ()
			{
				$('[data-csv-preview]').val(csv2json(filereader.result, true, true));
				
				json = csv2json(filereader.result, false, true);
				
				$('[data-csv-preview]').height( $('[data-csv-preview]').get(0).scrollHeight );
			};
			
			filereader.readAsText(file);
		};
		
		var upload = function ()
		{
			if (!json) return app.alert(templates.csv.file.json, 'error');
			
			$.post('/api/rows', 
			{
				rows: json,
				table: $('[data-csv-table]').val()
			}, 
			function (data, response)
			{
				data = data || {};
				
				if (response === 'success' && data.updated) 
				{
					app.alert(templates.csv.success.replace(':rows', data.updated), 'success');
					
					$('[data-csv-upload-reset]').trigger('click');
				}
				else app.alert(data.message || templates.csv.file.error, 'error');
				
			}, 'json');
			
			return false;
		};
		
		var tables = app.tables();
		
		/*
			Compile Tables and Fields
		*/
		
		var html = compile(tables, 'csv-tables-fields');
		
		$('[app-template-cdn="csv-table-fields"]').html(html);
		
		/*
			Event Handlers
		*/
		
		$('[data-csv-table]').on('click', function (e)
		{
			var element = this;
			
			$(element).addClass('active');
						
			return form(element);
		});
		
		$('[data-csv-upload-reset]').on('click', function (e)
		{
			$('[data-csv-form]').get(0).reset();
			
			json = null;
			
			$('[data-csv-preview]').removeAttr('style');
			
			$('[data-csv-table]').removeClass('active');
			
			$('.app-overlay-section').removeClass('hidden');
			
			return false;
		});
		
		$('[data-csv-upload-submit]').on('click', function (e)
		{
			return upload();
		});
		
		$('[data-csv-file-trigger]').on('click', function (e)
		{
			$('[data-csv-file]').trigger('click');
			
			return false;
		});
		
		$('[data-csv-file]').on('change', function (e)
		{
			$('[data-csv-file-trigger]').val(this.files[0].name);
			
			process();
		});
	};
	
	this.editor = function (options)
	{
		if (!options || !app.data[options]) return app.alert(templates.editor.options, 'error');
		
		if (!window.directusData.tables) return app.alert(templates.editor.error, 'error');
		
		var checkboxes = ['status', 'toggle'], currtable, 
			UI = ['primary_key', 'status', 'text_input', 'toggle', 'numeric', 'textarea'];
		
		var load = function (table, fields, limit, offset, sort, order)
		{
			limit = Number(limit || 100);
			offset = Number(offset || 0);
			sort = sort || 'id';
			order = order || 'ASC';
			
			currtable = table;
			
			var query = {
				limit: limit,
				columns: fields,
				offset: offset
			};
			
			query['order[:sort]'.replace(':sort', sort)] = order;
			query = json2query(query);
			
			$('[data-editor-search-container]').addClass('hidden');
			
			$('[data-search-input]').val('');
			
			$('[data-editor-tables]').html('');
			
			$.getJSON("/api/:version/tables/:table/rows?:query".replace(':version', version).replace(':table', table).replace(':query', query), function (data)
			{
				render(data, table, fields, limit, offset, sort, order);
			});
		};
		
		var render = function (data, table, fields, limit, offset, sort, order)
		{
			if (!data || !data.data) return app.alert(templates.editor.error, 'error');
			
			if (!$(data.data).get(0)) return app.alert(templates.editor.rows.replace(":table", table), 'success');
						
			/*
				Get table schema data from directus tbles data
			*/
			
			var tables = app.tables();
			
			tables = tables.filter(function (row)
			{
				return row.schema.name === table;
			});
			
			tables = tables[0];
			
			if (!tables || !tables.schema) return app.alert(templates.editor.error, 'error');
						
			/*
				Process columns and match against fields
			*/
			
			var columns = tables.schema.columns;
			
			fields = fields.split(',');
			
			columns = columns.filter(function (row)
			{
				return fields.indexOf(row.id) > -1;
			});
						
			/*
				Pagination: Check to see a load more button is required - pagination
			*/
			
			var total = data.meta.total_entries;
			var loaded = data.meta.total;
			var iterations = Math.ceil(total / loaded);
			var pagination = [];
			
			for (var i = 0; i < iterations; i++)
			{
				var currpage = offset ? (offset / limit) : offset;
				
				pagination.push({
					table: table,
					limit: limit,
					offset: limit * i,
					fields: fields.join(','),
					sort: sort,
					order: order,
					selected: currpage == i ? "selected disabled" : "",
					page: i + 1
				});
			}
			
			var json = {
				thead: columns,
				tbody: data.data,
				pagination: pagination
			};
			
			for (var a in json.tbody)
			{
				var rows = json.tbody[a];
				
				for (var i in rows) 
				{
					var row = rows[i];
					
					if (fields.indexOf(i) < 0)
					{
						delete rows[i];
						
						continue;
					}
					
					var value = row;
					var column = columns.filter(function (curr)
					{
						return curr.column_name === i;
					});
					column = column[0];
					column.column_ui = column.column_ui || column.ui;
					column.display_name = column.display_name || form.name(column.column_name);
					
					var checkbox = checkboxes.indexOf(column.ui) >= 0;
										
					rows[i] = {
						value: value,
						maxlength: column.length || 11,
						placeholder: column.options.attributes.placeholder || column.comment,
						display_name: column.display_name,
						readonly: column.key === 'PRI',
						checkbox: checkbox,
						checked: checkbox && value == 1 ? "checked" : "",
						options: column.options,
						column_name: column.column_name,
						column_type: column.type,
						column_ui: column.ui,
						key: column.key,
						id: column.id,
						column_ui: column.ui
					};	
				}
			};
			
			/*
				Render table rows and form inputs
			*/
			
			var html = compile(json, 'editor-tables-form');
			
			$('[data-editor-tables]').html(html);
			
			$('[data-editor-search-container]').removeClass('hidden');
			
			var html = compile(pagination, 'editor-tables-pagination');	
			
			$('[app-template-cdn="editor-tables-pagination"]').html(html);			
			
			form.handlers();
		};
		
		var submit = function ()
		{
			var processed = form.process();
			
			if (!processed || !processed.length) return app.alert(templates.editor.submit.rows, 'error');
			
			form.submit(currtable, processed, function (data, response)
			{
				if (response === 'success' && data.updated) 
				{
					app.alert(templates.editor.submit.success.replace(':rows', data.updated), 'success');					
				}
				else app.alert(data.message || templates.editor.submit.error, 'error');
			});	
		};
		
		options = app.data[options];
		
		/*
			Apply columns using fields
		*/
				
		var html = compile(options, 'editor-tables-fields');
			
		$('[app-template-cdn="editor-tables-fields"]').html(html);
		
		/*
			Event Handlers
		*/
		
		$('[data-editor-anchor]').on('click', function (e) 
		{
			$('[data-editor-anchor]').removeClass('active');
			
			$(this).addClass('active');		
			
			load(this.getAttribute('data-editor-table'), 
			this.getAttribute('data-editor-fields'), 
			this.getAttribute('data-editor-limit'), 
			this.getAttribute('data-editor-offset'), 
			this.getAttribute('data-editor-sort'), 
			this.getAttribute('data-editor-order'));
		});
		
		$('[data-editor-upload-submit]').on('click', function (e)
		{
			return submit();
		});
		
		$('[app-template-cdn="editor-tables-pagination"]').on('change', function (e)
		{
			var option = $(this).find('option:selected').get(0);
			
			load(option.getAttribute('data-editor-table'), 
			option.getAttribute('data-editor-fields'), 
			option.getAttribute('data-editor-limit'), 
			option.getAttribute('data-editor-offset'), 
			option.getAttribute('data-editor-sort'), 
			option.getAttribute('data-editor-order'));
		});	
	};

	this.init = function ()
	{
		ui();

		if (app.initialized || !window.directusData.user.data.token) return false;

		app.initialized = true;
		
		user = window.directusData.user || {};

		/*
			Listen for History Change - Navigation Event ...
		*/

		Router = Backbone.Router.extend();
		router = new Router();

		Backbone.history.on("all", function (route, router)
		{
			ui();
			
			if (window.location.pathname.indexOf('/users/') === 0) setTimeout(function () { ui() }, 2000);
		});

		/*
			Download APP.JSON - descriptions for each table, if applicable ...
		*/

		$.get("/philleepedit/app/templates/app.html", function (html)
		{
			$('body').append(html);

			$('[data-app-templates]').each(function (index, element)
			{
				var key = element.getAttribute('data-app-templates');

				templates[key] = $(element).html();
			});
		
			ui();
		});
	};

	/*
		Make sure user has an active session ...
	*/

	this.key = function ()
	{
		if (!window.directusData || !window.directusData.authenticatedUser) return null;

		return window.directusData.authenticatedUser.access_token;
	};

	/*
		Bulk uploads ...
		DEPENDENTS:
			Dropzone.js
	*/

	this.uploads = function ()
	{
		var $uploads = $('[data-multi-file-upload]'),
			myDropzone, album, joins = [], curr = 0;
			
		if (!app.key()) return app.alert(templates.uploads.error.apikey, 'error');

		if (!$uploads.get(0)) return false;

		var upload = function ()
		{
			/*
				STEP I: Upload and Create Files
			*/

			var files = function ()
			{
				var extension = file.name.toLowerCase();
					extension = extension.split('.').pop();

				var post = {
					data: file.dataURL,
					type: file.type,
					name: 'image.' + extension
				};

				$(file.previewElement).find('[data-multi-file-upload-input]').each(function (index, input)
				{
					post[input.name] = input.value;
				});

				app.alert(templates.uploads.status.processing.replace(':curr', curr + 1).replace(':length', myDropzone.files.length));

				$.post('/api/:version/files'.replace(':version', version), post, function (data, response)
				{
					$(file.previewElement).find('[data-multi-file-uploaded]').addClass('active animated zoomIn');

					if (response === 'success' && data.data && data.data.id) joins.push({
						file_id: data.data.id
					});

					curr++;

					upload();
				});
			};

			/*
				STEP II: Create album
			*/

			var createAlbum = function ()
			{
				/*
					Check if album will be created
				*/

				$('[data-multi-file-album-input]').each(function (index, input)
				{
					if (input.value)
					{
						album = album || {};

						album[input.name] = input.value;
					}
				});

				/*
					If no album, terminate process and reset ...
				*/

				if (!album)
				{
					$('[data-multi-file-upload-reset]').trigger('click');

					app.alert(templates.uploads.success.images.replace(':length', myDropzone.files.length));

					return false;
				}
				else app.alert(templates.uploads.status.album.replace(':title', album.title));

				$.post('/api/:version/tables/app_albums/rows'.replace(':version', version), album, function (data, response)
				{
					if (response === 'success' && data.data && data.data.id)
					{
						/*
							Process joins and add album id ...
						*/

						joins.forEach(function (row, index)
						{
							row.album_id = data.data.id;
						});

						albumFiles();
					}
					else app.alert(templates.uploads.error.album, "error");
				});
			};

			/*
				STEP III: Create Album Files - Join Table
			*/

			var albumFiles = function ()
			{
				$.post('/api/:version/tables/app_albums_files/rows/bulk'.replace(':version', version), { rows : joins }, function (data, response)
				{
					if (response === 'success')
					{
						app.alert(templates.uploads.success.album.replace(':title', album.title), 'success');

						$('[data-multi-file-upload-reset]').trigger('click');

						if (window.location.pathname.indexOf('albums')) router.navigate(window.location.pathname, {
							trigger: true
						});
					}
					else app.alert(templates.uploads.error.album, "error");
				});
			};

			var file = myDropzone.files[curr];

			if (!file && myDropzone.files.length) return createAlbum();
			else if (!file) return app.alert(templates.uploads.error.nofile, 'error');

			files();
		};

		$uploads.each(function (index, element)
		{
			myDropzone = new Dropzone(element,
			{
				url: '/api/:version/files'.replace(':version', version),
				clickable: true,
				addRemoveLinks: true,
				autoProcessQueue: false,
				thumbnailWidth: 200,
				thumbnailHeight: 200,
				thumbnailMethod: 'crop',
				maxFilesize: 5,
				acceptedFiles: '.jpg, .jpeg, .png, .gif',
				dictRemoveFile: "",
				previewTemplate: templates.dropzone,
				init: function (file, done)
				{
					this.on("thumbnail", function(file)
					{
						if (myDropzone.files.indexOf(file) === myDropzone.files.length - 1) app.alert("Thumbnails processed ... You may update images information or upload images ...", 'success');

						$(file.previewElement).find('.dz-details, .dz-crop-mark, .dz-remove-mark, .dz-success-mark').addClass('active animated fadeIn');

						$(file.previewElement).find('[data-multi-file-upload-edit]').on('click', function (e)
						{
							var $parent = $(this).parents('[data-multi-file-upload-item]');

							$('[data-multi-file-upload-form]').addClass('active animated fadeIn');

							$('[data-multi-file-upload-form-update]').one('click', function (e)
							{
								$parent.find('[data-multi-file-upload-input]').each(function (index, input)
								{
									input.value = $('[data-multi-file-upload-form]').find('[data-multi-file-upload-input]').filter(function () { return this.name === input.name }).val();

									$('[data-multi-file-upload-form]').removeClass('active animated fadeIn');
								});
							});

							$('[data-multi-file-upload-form-cancel]').one('click', function (e)
							{
								$('[data-multi-file-upload-form]').removeClass('active animated fadeIn');
							});
						});
					});

					this.on("complete", function(file)
					{
						myDropzone.removeFile(file);
					});

					this.on("drop", function(file)
					{
						app.alert("Please wait, processing image thumbnails ...");
					});
				}
			});
		});

		$('[data-multi-file-upload-reset]').on('click', function (e)
		{
			myDropzone.removeAllFiles(true);

			$('[data-multi-file-upload-form]').removeClass('active animated fadeIn');

			$('[data-multi-file-upload-input]').val('');
		});

		$('[data-multi-file-upload-submit]').on('click', function (e)
		{
			upload(0);
		});
	};
};

APP.APPTIMER = setInterval(function (e)
{
	/*
		Load APP only when all dependents have loaded ...
	*/
	
	if (window.Dropzone && Dropzone.autoDiscover) Dropzone.autoDiscover = false;

	if (window.$ && window.Backbone && window.require && window.noty && window.Handlebars)
	{
		window.app = new APP();

		app.init();

		clearInterval(APP.APPTIMER);
	}
},
500);
