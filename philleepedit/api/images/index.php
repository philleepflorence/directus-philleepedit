<?php

ini_set('memory_limit','512M');
set_time_limit(300);

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

require_once ('../../vendors/rappasoft/laravel-helpers/src/helpers.php');
require_once ('../../vendors/php-image-resize/ImageResize.php');

use \Eventviva\ImageResize;

class IMAGES
{
    /*
        Resize Animated GIFs
        All other types use the RESIZE method
        DEPENDENTS:
            Imagick
    */

    public static $SIZES = 0;

    public static function GIF ($source, $destination, $width = 0, $height = 0, $orientation = 'landscape')
    {
        # Check if Imagick Library is installed

        if (!extension_loaded('Imagick')) exit(json_encode(["error" => "Server Error - Imagick Image Library has not been installed!"]));

        # Create new Imagick class

        $imagick = new \Imagick($source);

        # Create directory if it does not exists

        IMAGES::DIRECTORY($destination);

        # Split gif animation into image

        $imagick = $imagick->coalesceImages();

        if ($orientation === 'landscape') $height = 0;
        else $width = 0;

        try
        {
            # Crop images one by one

            do
            {
                /*
                    First parameter is resized image width and second is resized image height
                    If one of the params is set to zero, image will keep the aspect ratio
                */
                $imagick->resizeImage($width, $height, \Imagick::FILTER_BOX, 1);
            }
            while ($imagick->nextImage());

            # Reassemble the animation

            $imagick = $imagick->deconstructImages();

            # Save gif animation to destination

            $imagick->writeImages($destination, true);
        }
        catch (Exception $e)
        {
            # If any errors, removed new path and exit application

            unlink($destination);

            exit(json_encode(["error" => "Server Error - " . $e->getMessage()]));
        }

        return file_exists($destination);
    }

    /*
        Process image hosted in CDN
    */

    public static function IMAGE ($params = [], $return = null)
    {
        $image = array_get($params, 'image');
        $width = array_get($params, 'width');
        $height = array_get($params, 'height') ?: $width;
        $size = array_get($params, 'size');

        # Get image properties

        try
        {
            $imagesize = @getimagesize($image);
        }
        catch (Exception $e)
        {
            if (!$return):

                IMAGES::RENDER($image);

            else:

                return null;

            endif;
        }

        # If image is a thumbnail, return thumbnail

        if (stripos($image, 'uploads/thumbs')) IMAGES::RENDER($image, array_get($imagesize, 'mime'));

        # Allowed sizes - try to match input width and height

        $sizes = IMAGES::SIZES();
        $minsize = array_get($sizes, 'xs');

        $extensions = IMAGES::EXTENSIONS();
        $thumbfolder = '/uploads/thumbs/';
        $folder = '';

        # If no image exit application

        if (!$image && !$return) exit(json_encode(["error" => "Malformed request syntax - Missing Image Property!"]));
        else if (!$image && $return) return null;

        # Trim white space if applicable

        $image = trim($image);

        # Replace domain for real path

        if (stripos($image, IMAGES::HOST()) >= 0):

            $path = explode(IMAGES::HOST(), $image);
            $path = end($path);

        else:

            $path = $image;

        endif;

        $realpath = IMAGES::ROOT() . $path;

        # If image does not exist - exit application

        $fileexists = file_exists($realpath);

        if (!$fileexists && !$return) exit(json_encode(["error" => "Malformed request syntax - Image does not exist!"]));
        else if (!$fileexists && $return) return null;

        # Get path info

        $pathinfo = pathinfo($realpath);

        # Make sure image extension is valid

        $valid = in_array(strtolower( array_get($pathinfo, 'extension', '') ), $extensions);

        if (!$valid && !$return) exit(json_encode(["error" => "Malformed request syntax - Invalid Image Type!"]));
        else if (!$valid && $return) return null;

        $orientation = array_get($imagesize, 'width') > ( array_get($imagesize, 'height', 1) * 0.7 ) ? 'landscape' : 'portrait';

        # Get the appropriate size or return original

        if ($size):

            $size = array_get($sizes, $size) ?: array_get($sizes, 'xs');

        elseif ($width || $height):

            /*
                Return thumbnail if image is less than xs
            */

            $thumbnail = $thumbfolder . ltrim(array_get($pathinfo, 'basename', ''), '0');

            if ($width < array_get($minsize, 'width') && $height < array_get($minsize, 'height') && file_exists(IMAGES::ROOT() . $thumbnail)) IMAGES::RENDER(IMAGES::DOMAIN() . $thumbnail, array_get($imagesize, 'mime'));

            /*
                TODO: Ideally we should be looking ahead and rounding to the nearest with or height
                EXAMPLE: 1300 with should match 1280 size and not 1600
            */

            foreach($sizes as $key => $value):

                $WIDTH = array_get($value, 'width') + array_get($value, 'offset', 0);
                $HEIGHT = array_get($value, 'height') + array_get($value, 'offset', 0);

                if (( $orientation === 'landscape' && $WIDTH >= $width ) || ( $orientation === 'portrait' && $HEIGHT >= $height )):

                    $size = $value;

                    break;

                endif;

            endforeach;

        else:

            if (!$return):

                IMAGES::RENDER($image, array_get($imagesize, 'mime'));

            else:

                return null;

            endif;

        endif;

        if (!array_get($size, 'size') && !$return) IMAGES::RENDER($image, array_get($imagesize, 'mime'));
        else if (!array_get($size, 'size') && $return) return null;

        $destination = array_get($pathinfo, 'dirname') . ( '/' . array_get($size, 'size') . '/' ) . array_get($pathinfo, 'basename');

        array_set($size, 'orientation', $orientation);

        $new = IMAGES::RESIZE($realpath, $destination, $size, [
            "pathinfo" => $pathinfo,
            "imagesize" => $imagesize,
        ]);

        if ($new && $return) return [
            "source" => $image,
            "new" => $new
        ];

        if ($new) IMAGES::RENDER($new, array_get($imagesize, 'mime'));
        else exit(json_encode(["error" => "Server Error - Unable to create size!"]));
    }

    /*
        Process all images hosted in CDN
    */

    public static function PROCESS ()
    {
        $realpath = IMAGES::ROOT() . '/uploads';
        $paths = scandir($realpath);
        $domain = IMAGES::DOMAIN();
        $extensions = IMAGES::EXTENSIONS();
        $sizes = IMAGES::SIZES();
        $images = [];
        $resized = [];

        # Get only the images in the uploads root

        foreach ($paths as $filename):

            if ( !strpos(basename($filename), '.') ) continue;

            $file = $realpath . $filename;
            $extension = strtolower( pathinfo($file, PATHINFO_EXTENSION) ?: '' );

            if (in_array($extension, $extensions)) array_push($images, $domain . '/uploads/' . $filename);

        endforeach;

        # Process each image with the different sizes where applicable

        foreach ($images as $image):

            foreach ($sizes as $size):

                $created = IMAGES::IMAGE([
                    "image" => $image,
                    "size" => array_get($size, 'size')
                ], true);

                array_push($resized, [
                    "image" => $image,
                    "resized" => $created,
                    "size" => array_get($size, 'size')
                ]);

            endforeach;

        endforeach;

        die(json_encode([
            "data" => [
                "resized" => $resized,
                "images" => $images
            ],
            "meta" => [
                "resized" => IMAGES::$SIZES,
                "images" => count($images)
            ]
        ]));
    }

    public static function RENDER ($image = '', $mime = 'image/jpeg')
    {
        header('Content-Type: ' . $mime);
        header('Location: ' . $image);

        die();
    }

    /*
        IMAGES::RESIZE()
        Resize image if applicable
        DESTINATION FOLDER MUST BE WRITABLE!
        DEPENDENTS:
            GD Library
            ImageResize Vendor
        ARGUMENTS:
            $source: realpath of existing image
            $destination: realpath of new resized image
            $size: Array with orientation, width, and height
            $params: [
                "pathinfo" => pathinfo(),
                "size" => $size,
                "imagesize" => getimagesize()
            ]
    */

    public static function RESIZE ($source, $destination, $size, $params = [])
    {
        $new = IMAGES::DOMAIN() . str_ireplace(IMAGES::ROOT(), '', $destination);
        $curr = IMAGES::DOMAIN() . str_ireplace(IMAGES::ROOT(), '', $source);

        $imagesize = array_get($params, 'imagesize') ?: getimagesize($source);
        $pathinfo = array_get($params, 'pathinfo') ?: pathinfo($source);
        $orientation = array_get($size, 'orientation', 'landscape');
        $extension = strtolower( array_get($pathinfo, 'extension', 'jpg') );

        # Check if resize if necessary - if not return original image

        if (( $orientation === 'landscape' && array_get($imagesize, 0) < array_get($size, 'width') ) || ( $orientation === 'portrait' && array_get($imagesize, 1) < array_get($size, 'height') )) return $curr;

        # If resize already exists - render and exit application

        if (file_exists($destination)) return $new;

        # If GIF use GIF Method

        if ($extension === 'gif'):

            IMAGES::GIF($source, $destination, $width, $height, $orientation);

        else:

            # Check if GD Library is installed

            if (!extension_loaded('GD')) exit(json_encode(["error" => "Server Error - GD Image Library has not been installed!"]));

            # Create directory if it does not exists

            IMAGES::DIRECTORY($destination);

            # Resize and save image to destination

            $image = new ImageResize($source);

            try
            {
                if ($orientation === 'landscape') $image->resizeToWidth(array_get($size, 'width'));
                else $image->resizeToHeight(array_get($size, 'height'));

                $image->save($destination);

                IMAGES::$SIZES++;
            }
            catch (ImageResizeException $e)
            {
                # If any errors, removed new path and exit application

                unlink($destination);

                exit(json_encode(["error" => "Server Error - " . $e->getMessage()]));
            }

        endif;

        if (file_exists($destination)) return $new;
        else return null;
    }

    # Helper Methods

    public static function DIRECTORY ($destination, $touch = true)
    {
        $directory = dirname($destination);

        if (!is_dir( $directory )) mkdir( $directory, 0777 );

        if (!file_exists($destination) && $touch) touch($destination);

        return file_exists($destination);
    }

    public static function ROOT ()
    {
        return dirname(dirname(__DIR__));
    }

    public static function PROTOCOL ()
    {
        return ( array_get($_SERVER, 'HTTPS') && array_get($_SERVER, 'HTTPS') !== 'off' ) ? "https://" : "http://";
    }

    public static function HOST ()
    {
        return array_get($_SERVER, 'HTTP_HOST') ?: array_get($_SERVER, 'SERVER_NAME');
    }

    public static function EXTENSIONS ()
    {
        return ['jpg', 'jpeg', 'png', 'gif'];
    }

    public static function DOMAIN ()
    {
        return IMAGES::PROTOCOL() . IMAGES::HOST();
    }

    public static function SIZES ($size = null)
    {
        $sizes = [
            "xs" => [
                "width" => 480,
                "height" => 480,
                "offset" => 130,
                "size" => "xs"
            ],
            "sm" => [
                "width" => 640,
                "height" => 640,
                "offset" => 180,
                "size" => "sm"
            ],
            "md" => [
                "width" => 960,
                "height" => 960,
                "offset" => 200,
                "size" => "md"
            ],
            "lg" => [
                "width" => 1280,
                "height" => 1280,
                "offset" => 200,
                "size" => "lg"
            ],
            "xl" => [
                "width" => 1600,
                "height" => 1600,
                "offset" => 150,
                "size" => "xl"
            ],
            "xxl" => [
                "width" => 1900,
                "height" => 1900,
                "offset" => 0,
                "size" => "xxl"
            ]
        ];

        if ($size) return array_get($sizes, $size);
        else return $sizes;
    }
}

if (!array_get($_GET, 'image')) IMAGES::PROCESS();
else IMAGES::IMAGE($_GET);
