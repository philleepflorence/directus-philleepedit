<?php

include_once(BASE_PATH . "/customs/helpers/app.php");

use Directus\Bootstrap;
use Directus\View\JsonView;
use Directus\Database\TableGateway\RelationalTableGateway;

use Directus\Util\ArrayUtils;
use Directus\Util\DateUtils;
use Directus\Util\StringUtils;

$app = Bootstrap::get('app');

$app->post('/rows', function ()
{
	include_once(BASE_PATH . "/api/api.configuration.php");
    
    # If no configuration exit applicaiton
    
    if (!isset($configuration)) die("Missing configuration!");
    
	$table = ArrayUtils::get($_REQUEST, 'table');
    $rows = ArrayUtils::get($_REQUEST, 'rows');
    
    /*
	    Initialize connection and table instance
    */
    
    $dbConnection = Bootstrap::get('zendDb');
    $acl = Bootstrap::get('acl');
    $tableGateway = new RelationalTableGateway($table, $dbConnection, $acl);
    $tableSchema = $tableGateway->getTableSchema();
    $getColumns = $tableSchema->getColumns();
    $types = ArrayUtils::get($configuration, 'endpoints.rows.types');
    $primary = 'id';
    $uploaded = 0;
    $valid = [];
    $columns = [];
    
    /*
	    Process columns to get required & validation method - defaults to is_string
    */
    
    foreach ($getColumns as $column):
    
    	$column = json_decode(json_encode($column), true);
    
    	$type = ArrayUtils::get($column, 'type');
    	$name = ArrayUtils::get($column, 'name');
    	
    	if (ArrayUtils::get($column, 'key') == "PRI") $primary = $name;
    	
    	$columns[$name] = [
	    	"method" => ArrayUtils::get($types, $type) ?: "is_string",
	    	"required" => ArrayUtils::get($column, 'required'),
	    	"validation" => [
		    	"pattern" => ArrayUtils::get($column, 'options.validation_string'),
		    	"message" => ArrayUtils::get($column, 'options.validation_message'),
		    	"type" => ArrayUtils::get($column, 'options.validation_type', 'rgx')
	    	]
    	];
    
    endforeach;
    
    /*
	    Process CSV and validate each row -> column/value
    */
    
    foreach ($rows as $row):
    
    	foreach ($row as $field => $value):
    	
    		/*
	    		Get method and validate - omit all rows with one invalid value
    		*/
			
			$column = ArrayUtils::get($columns, $field);
    		$method = ArrayUtils::get($columns, "method");
    		$required = ArrayUtils::get($columns, "required");
    		$value = trim($value);    		
    		
    		/*
	    		Check for numbers and floats and force type for validation
    		*/
    		
    		if (is_string($value) && ctype_digit($value)) $value = intval($value);
    		
    		if ($required && !$value) continue 2;
    		
    		if ($method && function_exists($method) && !$method($value)) continue 2;
    		
    		/*
	    		Pass through validation if applicable
    		*/
    		
    		$pattern = ArrayUtils::get($column, 'validation.pattern');
    		$message = ArrayUtils::get($column, 'validation.message');
    		
    		if (ArrayUtils::get($column, 'validation.type') === 'rgx' && $pattern && $message && $value)
    		{
	    		$pattern = substr($pattern, 0) === substr($pattern, -1) ? $pattern : "/{$pattern}/";
	    		
	    		if (PHILLEEPEDIT::REGEXP($pattern) && !preg_match($pattern, $value))
	    		{
		    		return JsonView::render([
			    		"error" => $message
		    		]);
	    		}
    		}
    	
    	endforeach;
    	
    	array_push($valid, $row);
    
    endforeach;
    
    /*
	    Insert or update each row depending on $primary - tracking each successful transaction
    */
    
    foreach ($valid as $row):
    
    	$result = $tableGateway->updateRecord($row);
    	
    	if ($result) $uploaded++;
    
    endforeach;
    
    return JsonView::render([
	    "success" => $uploaded ? true : false,
	    "updated" => $uploaded,
	    "valid" => count($valid)
    ]);
});
