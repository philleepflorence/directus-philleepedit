<?php

include_once(BASE_PATH . "/customs/helpers/app.php");

use Directus\Bootstrap;
use Directus\View\JsonView;
use Directus\Database\TableGateway\RelationalTableGateway;

use Directus\Util\ArrayUtils;
use Directus\Util\DateUtils;
use Directus\Util\StringUtils;

$app = Bootstrap::get('app');

$app->get('/preview', function ()
{
    $env = [
        'development' => [
            'domain' => APP_URI
        ],
        'staging' => [
            'domain'=> APP_STAGING_URI
        ],
        'production' => [
            'domain'=> APP_PRODUCTION_URI
        ]
    ];
    $path = ArrayUtils::get($_GET, 'path');
    $domain = ArrayUtils::get($env, DIRECTUS_ENV . '.domain');

    header('Location: ' . ($domain . $path));

    die();
});
