<?php

include_once(BASE_PATH . "/customs/helpers/app.php");

use Directus\Bootstrap;
use Directus\View\JsonView;
use Directus\Database\TableGateway\RelationalTableGateway;

use Directus\Util\ArrayUtils;
use Directus\Util\DateUtils;
use Directus\Util\StringUtils;

$app = Bootstrap::get('app');

$app->get('/editable', function ()
{
	# See session -> prefix in api/configuration.php for session prefix
	
	$token = ArrayUtils::get($_SESSION, 'api_auth_user.access_token');
    
    if (!$token) die("You must be logged in to continue!");
    
    $token = APP_URI . "/admin/{$token}";
    
    header('Location: ' . $token);
    
    die();
});
