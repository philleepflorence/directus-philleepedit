<?php

include_once(BASE_PATH . "/customs/helpers/app.php");

use Directus\Bootstrap;
use Directus\View\JsonView;
use Directus\Database\TableGateway\RelationalTableGateway;

use Directus\Util\ArrayUtils;
use Directus\Util\DateUtils;
use Directus\Util\StringUtils;

$app = Bootstrap::get('app');

/*
    Get media files available via the CDN
    Endpoint: http://[domain]/api/cdn?access_token=[access_token]
*/

$app->get('/cdn', function ()
{
    include_once(BASE_PATH . "/api/api.configuration.php");
    
    # If no configuration exit applicaiton
    
    if (!isset($configuration)) die("Missing configuration!");
    
	$sizes = ArrayUtils::get($configuration, 'endpoints.cdn.sizes');
    $images = ArrayUtils::get($configuration, 'endpoints.cdn.extensions');
    $directories = [
        'app' => [
            "realpath" => realpath (BASE_PATH . '/storage/app/media'),
            "subfolder" => "app/media"
        ],
        'uploads' => [
            "realpath" => realpath (BASE_PATH . '/storage/uploads'),
            "subfolder" => "uploads"
        ]
    ];
    $data = [
        "app" => [
            "template" => 'cdn-app-media',
            "data" => []
        ],
        "sizes" => [
            "template" => 'cdn-image-sizes',
            "data" => $sizes
        ],
        "uploads" => [
            "template" => 'cdn-uploads-media',
            "data" => []
        ],
        "resize" => [
            "url" => APP_CDN_URI . "/api/images/index.php"
        ]
    ];

    # Loop through all applicable media sub directories ...

    foreach ($directories as $index => $directory):

        $path = ArrayUtils::get($directory, 'realpath');
        $objects = scandir($path);
        $subfolder = ArrayUtils::get($directory, 'subfolder');
        $folder = "/{$subfolder}";

        # Loop through files in the root of current directory

        foreach ($objects as $file):

            # Get only actual files and not folders ...

            if (!strpos($file, '.') || is_dir($file)) continue;

            $realpath = "{$path}/{$file}";

            $filename = APP_CDN_URI . "{$folder}/{$file}";

            $pathinfo = pathinfo($realpath);
            $extension = ArrayUtils::get($pathinfo, 'extension');

            $dirname = ArrayUtils::get($pathinfo, 'dirname');
            $basename = ArrayUtils::get($pathinfo, 'filename');

            # Get sizes ...

            $currsizes = [];
            $currfilename = null;

            foreach($sizes as $size):

                $dir = ArrayUtils::get($size, 'size');

                # Trim leading zeros for thumbs ...

                $trim = ArrayUtils::get($size, 'trim');

                $currbase = is_numeric($trim) && stripos($basename, "{$trim}") === 0 ? ( $basename + $trim ) : $basename;

                # Check if resized image exists - display smallest possible image ...

                $currfile = "{$dirname}/{$dir}/{$currbase}.{$extension}";

                $currurl = APP_CDN_URI . "{$folder}/{$dir}/{$currbase}.{$extension}";

                if (!$currfilename && is_file($currfile)) $currfilename = $currurl;

                if (is_file($currfile)) ArrayUtils::set($currsizes, "sizes.{$dir}", [
                    "image" => $currfile,
                    "url" => $currurl,
                    "folder" => $dir
                ]);

            endforeach;

            $pathinfo = array_merge($pathinfo, [ 'url' => $currfilename ?: $filename, "original" => $filename ], $currsizes);

            if (in_array(strtolower($extension), $images)) $pathinfo['image'] = true;

            array_push($data[$index]['data'], $pathinfo);

        endforeach;

    endforeach;

    # Return JSON ...

    die(json_encode($data));
});
