<?php

use Directus\Bootstrap;
use Directus\View\JsonView;
use Directus\Database\TableGateway\RelationalTableGateway;

use Directus\Util\ArrayUtils;
use Directus\Util\DateUtils;
use Directus\Util\StringUtils;

$app = Bootstrap::get('app');

/*
    Get all Page pages
    These would be controlled by the page model of the application
    Endpoint: http://[domain]/api/pages?access_token=[access_token]
*/

$app->get('/pages', function ()
{
    /*
        Check if Cache exists before quering DB
    */

    $filename = BASE_PATH . "/philleepedit/app/cache/pages.json";

    $reload = ArrayUtils::get($_GET, 'adminToken') ?: DIRECTUS_ENV === 'development' ?: ArrayUtils::get($_GET, 'reload');

    $cache = !$reload ? FILESYSTEM::GET($filename, true) : null;

    if ($cache) return JsonView::render($cache);

    include_once(BASE_PATH . "/api/api.configuration.php");
    
    # If no configuration exit applicaiton
    
    if (!isset($configuration)) die("Missing configuration!");
    
	$tables = ArrayUtils::get($configuration, 'endpoints.pages.tables');
    $dbConnection = Bootstrap::get('zendDb');
    $data = [];
    $params = [
        'depth' => 1,
        'status' => ArrayUtils::get($_GET, 'status', 1),
        'preview' => ArrayUtils::get($_GET, 'preview')
    ];
    $cdn = APP_CDN_URI . '/';

    /*
        Loop through tables and append result of getItems() to data object.
        Update images to CDN
    */

    foreach ($tables as $key => $value):

        $table = new RelationalTableGateway($value, $dbConnection);

        $entries = $table->getItems($params);
        
        $entries = CUSTOMHOOKS::EDITABLE($entries);

        $data[str_ireplace('pages_', '', $value)] = $entries;

    endforeach;

    /*
        Replace /storage/ with CDN domain
    */

    array_walk_recursive($data, function (&$value, $key) use ($cdn)
    {
        if (is_string($value) && strpos($value, '/storage/') === 0) $value = str_ireplace('/storage/', $cdn, $value);
    });

    /*
        Cache Data
    */

    FILESYSTEM::SET($filename, json_encode($data));

    return JsonView::render($data);
});
