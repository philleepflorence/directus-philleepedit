<?php

include_once(BASE_PATH . "/customs/helpers/app.php");
include_once(BASE_PATH . "/customs/helpers/filesystem.php");

use Directus\Bootstrap;
use Directus\View\JsonView;
use Directus\Database\TableGateway\RelationalTableGateway;

use Directus\Util\ArrayUtils;
use Directus\Util\DateUtils;
use Directus\Util\StringUtils;

$app = Bootstrap::get('app');

/*
    Get all App Tables and Consolidate
    These would be controlled by the app model of the application
    Endpoint: http://[domain]/api/app?access_token=[access_token]
*/

$app->get('/app', function ()
{
    /*
        Check if Cache exists before quering DB
    */

    $filename = BASE_PATH . "/philleepedit/app/cache/app.json";
    
    $reload = ArrayUtils::get($_GET, 'adminToken') ?: DIRECTUS_ENV === 'development' ?: ArrayUtils::get($_GET, 'reload');

    $cache = !$reload ? FILESYSTEM::GET($filename, true) : null;

    if ($cache) return JsonView::render($cache);

    include_once(BASE_PATH . "/api/api.configuration.php");
    
    # If no configuration exit applicaiton
    
    if (!isset($configuration)) die("Missing configuration!");
    
	$tables = [
	    'admin' => [
		    'table' => 'directus_users',
			"params" => [
				'filters' => [
				    'token' => ArrayUtils::get($_GET, 'adminToken')
			    ]
			]
	    ]
    ];
    
    if (isset($configuration) && is_array(ArrayUtils::get($configuration, 'endpoints.app.tables'))) $tables = array_merge($tables, ArrayUtils::get($configuration, 'endpoints.app.tables'));
    
    $dbConnection = Bootstrap::get('zendDb');
    $data = [];
    $params = [
        'depth' => 1,
        'status' => ArrayUtils::get($_GET, 'status', 1),
        'preview' => ArrayUtils::get($_GET, 'preview')
    ];
    $cdn = APP_CDN_URI . '/';

    /*
        Loop through tables and append result of getItems() to data object.
        Update images to CDN
    */

    foreach ($tables as $key => $value):

        $table = new RelationalTableGateway($value['table'], $dbConnection);

        $entries = $table->getItems(array_merge($params, ArrayUtils::get($value, 'params', [])));
        
        $entries = CUSTOMHOOKS::EDITABLE($entries);

        $data[$key] = $entries;

    endforeach;
    
    /*
	    Set Admin User
    */
    
    $admin = ArrayUtils::get($_GET, 'adminToken') ? ArrayUtils::get($data, 'admin.data.0') : NULL;
    
    ArrayUtils::set($data, 'admin', $admin);

    /*
        Replace /storage/ with CDN domain
    */

    array_walk_recursive($data, function (&$value, $key) use ($cdn)
    {
        if (is_string($value) && strpos($value, '/storage/') === 0) $value = str_ireplace('/storage/', $cdn, $value);
    });

    /*
        Cache Data - remove Admin User
    */
    
    $cache = $data;
    
    if (isset($cache['admin'])) unset($cache['admin']);

    FILESYSTEM::SET($filename, json_encode($cache));

    return JsonView::render($data);
});
