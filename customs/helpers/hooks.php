<?php

include_once(BASE_PATH . "/customs/helpers/app.php");

use Directus\View\JsonView;

use Directus\Util\ArrayUtils;
use Directus\Util\DateUtils;
use Directus\Util\StringUtils;

class CUSTOMHOOKS
{
	/*
        Add content editable link to each row
    */
    	
	public static function EDITABLE($entries = [])
	{
		$table = ArrayUtils::get($entries, 'meta.table');
		$data = ArrayUtils::get($entries, 'data');
		
		if (!$table || !$data) return $entries;
				
		$protocol = stripos(ArrayUtils::get($_SERVER, 'SERVER_PROTOCOL'),'https') === true ? 'https://' : 'http://';
        $domain = $protocol . ( ArrayUtils::get($_SERVER, 'HTTP_HOST') ?: ArrayUtils::get($_SERVER, 'SERVER_NAME') );     
        
        if (ArrayUtils::get($data, 0)):
        
        	# Multiple rows - get id to form editable link
		
			foreach($data as $key => $row):
			
				$id = ArrayUtils::get($row, 'id');
				
				if ($id) $data[$key]['contenteditable'] = "{$domain}/tables/{$table}/{$id}";
							
			endforeach;
			
		else:
		
			# Single row - no need for id when making edits
			
			$data['contenteditable'] = "{$domain}/tables/{$table}";
		
		endif;
		
		$entries['data'] = $data;
		
		return $entries;
	}
	
    /*
        Update response object
    */

    public static function RESPONSE($payload = null)
    {
        $data = $payload->getData();
        $protocol = stripos(ArrayUtils::get($_SERVER, 'SERVER_PROTOCOL'),'https') === true ? 'https://' : 'http://';
        $domain = $protocol . ( ArrayUtils::get($_SERVER, 'HTTP_HOST') ?: ArrayUtils::get($_SERVER, 'SERVER_NAME') );
		$cdn = APP_CDN_URI . '/';
        $table = ArrayUtils::get($data, 'meta.table');

        if (!ArrayUtils::get($data, 'data') || strpos($table, 'directus') === 0) return $payload;
        
        # Get editable links
        
        $data = CUSTOMHOOKS::EDITABLE($data);        

        /*
            Replace /storage/ with CDN domain
        */

        array_walk_recursive($data, function (&$value, $key) use ($cdn)
        {
            if (is_string($value) && strpos($value, '/storage/') === 0) $value = str_ireplace('/storage/', $cdn, $value);
        });

        $payload->replace($data);

        return $payload;
    }

    /*
        Update cache on every table update
    */

    public static function UPDATE($table = NULL, $record)
    {
        $caches = [
            "app_" => BASE_PATH . "/storage/app/cache/app.json",
            "contents_" => BASE_PATH . "/storage/app/cache/contents.json",
            "app_" => BASE_PATH . "/storage/app/cache/home.json",
            "contents_" => BASE_PATH . "/storage/app/cache/home.json",
            "pages_" => BASE_PATH . "/storage/app/cache/home.json",
            "pages_" => BASE_PATH . "/storage/app/cache/pages.json"

        ];

        /*
            The prefix or tablename must be in the $table to empty cache.
        */

        foreach ($caches as $key => $filename) if (stripos($table, $key) === 0 && file_exists($filename)) unlink($filename);

        return $table;
    }
}