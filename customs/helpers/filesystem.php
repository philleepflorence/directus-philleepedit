<?php

class FILESYSTEM
{
    public static function GET($filename = null, $json = null)
    {
        if (!is_file($filename)) return null;

        $contents = file_get_contents($filename);

        if (!$json) return $contents;

        try
        {
            $contents = json_decode($contents, true);
        }
        catch (Exception $e)
        {
            return null;
        }

        return $contents;
    }

    public static function SET($filename = null, $data = null, $permission = 0775)
    {
        if (!$filename) return null;

        $folder = dirname($filename);

        if (!is_dir($folder)) @mkdir($folder, $permission);

        if (!is_dir($folder)) return null;

        $file = @fopen($filename, "w+");

        @fwrite($file, $data);

        @fclose($file);
        
        @chmod($filename, $permission);

        return true;
    }
}
