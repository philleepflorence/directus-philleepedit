<?php

use Directus\Util\ArrayUtils;
use Directus\Util\DateUtils;
use Directus\Util\StringUtils;

class PHILLEEPEDIT
{
    public static function CALLER() # Development Tool - Returns the caller of a method
	{
		$callers = debug_backtrace();
		$function = ArrayUtils::get($callers, '2.function');
		$class = ArrayUtils::get($callers, '2.class');
		$type = ArrayUtils::get($callers, '2.type');

		return ( $class ? $class . $type . $function : $function ) . '()';
	}

    public static function DEBUG() # Development Tool - Prints data to screen. To force application to exit, set last argument to QUIT
	{
		$args = func_get_args();

		reset($args);

		print '<pre>';

		print "\n";

		print sprintf('DEBUGGER: %s', SELF::CALLER());

		print "\n";

		foreach($args as $arg):

			print "\n";

		    print_r(gettype($arg) === 'boolean' ? json_encode($arg) : $arg);

		    print "\n";

		endforeach;

		print "- \n";

		print '</pre>';

		exit();
	}
	
	public static function JSON()
    {
	    $args 		= func_get_args();
	    
	    die(json_encode($args));
    }
	
	public static function LOG()
    {
	    $args 		= func_get_args();
	    $log		= [];
	    $caller		= PHILLEEPEDIT::caller();
	    
	    foreach($args as $arg)
	    	$log[]	= stripcslashes(( json_encode($arg) ));
	    	
	    error_log($caller . ' - ' . implode(" . ", $log));
    }
    
    public static function REGEXP($pattern = '')
    {
	    return @preg_match($pattern, NULL) !== FALSE;
    }
}
