<?php
	$config = '../api/config.php';
	
	if (!file_exists($config)) die("<center>Missing configuration file! Please contact webmaster!</center>");
	
	require_once ('../api/config.php');
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title><?= APP_PROJECT_NAME ?> - Content Delivery Network</title>
		<meta name="description" content="<?= APP_PROJECT_NAME ?> - Content Delivery Network">
		<meta name="author" content="Philleep Florence LLC - 2018">

        <link rel="shortcut icon" href="/app/media/favicon.png"/>
        <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600" rel="stylesheet">
        <link href='/app/styles/animate.css' rel='stylesheet' type='text/css'>
        <style type="text/css">
            html, body {
                overflow: hidden;
                font-family: 'Nunito', sans-serif;
                min-width: 100vw;
                min-height: 100vh;
            }
            #wrapper {
                display: table;
                width: 100vw;
                height: 100vh;
            }
            #wrapper img {
                max-width: 300px;
                height: auto;
                display: block;
                margin: 2% auto;
            }
            .table-cell {
                display: table-cell;
                width: 100%;
                text-align: center;
                vertical-align: middle;
            }
            h1 {
                font-weight: 300;
				margin-bottom: 5%;
            }
			.files {
				padding: 5px;
			}
			footer {
				text-align: center;
				padding: 20px 0;
			}
			footer a.btn, footer a.btn:visited {
				display: inline-block;
				background: gray;
				color: white;
				font-weight: 500;
				text-transform: uppercase;
				text-decoration: none;
				padding: 10px 20px;
			}
			footer a.btn:hover, footer a.btn:active {
				background: #99c248;
			}
        </style>
	</head>
	<body>
		<div id="wrapper">
            <section class="table-cell animated bounceInDown">
                <a href="/"><img src="/app/media/logo.png" alt="<?= APP_PROJECT_NAME ?> - Content Delivery Network"></a>
                <h1><?= APP_PROJECT_NAME ?> - Content Delivery Network</h1>
				<p>
				<?php
					require_once ('vendors/rappasoft/laravel-helpers/src/helpers.php');

					$host = array_get($_SERVER, 'HTTP_HOST') ?: array_get($_SERVER, 'SERVER_NAME');

					$path = realpath ('uploads');
					$objects = new RecursiveIteratorIterator (new RecursiveDirectoryIterator ($path), RecursiveIteratorIterator::SELF_FIRST);
					$files = [];

					foreach ($objects as $name => $object):

						if (!strpos(basename($name), '.') || strpos($name, '/thumbs/')) continue;

						$pathinfo = pathinfo($name);
						$extension = strtolower(array_get($pathinfo, 'extension'));
						$basename = array_get($pathinfo, 'basename');

						if (!array_get($files, $extension)) array_set($files, $extension, []);

						array_push($files[$extension], $basename);

					endforeach;

					foreach ($files as $extension => $file):
				?>
				<span class="files"><b><?= strtoupper($extension) ?>s</b> <?= count($file) ?>.</span>
				<?php
					endforeach;
				?>
				</p>
				<footer>
					<a class="btn" href="//<?= str_ireplace('cdn.', 'cms.', $host) ?>/files">Manage Files</a>
				</footer>
            </section>
        </div>
	</body>
</html>
