<?php

/*
	App Globals 
	Add these with the respective URLs, these can change with environment
	APP_CDN_PATH - Internal Relative Path to CDN Folder - required if CDN is within CMS folder
	APP_CDN_URI - CDN URI for static assets
	APP_CMS_URI - Current CMS URI
	APP_URI - Main App URI - Defaults to development
	APP_STAGING_URI - Main App Staging URI
	APP_PRODUCTION_URI - Main App Production URI
*/

define('APP_PROJECT_NAME', '');
define('APP_CDN_PATH', '/storage');
define('APP_CDN_URI', ''); 
define('APP_CMS_URI', '');
define('APP_URI', '');
define('APP_STAGING_URI', '');
define('APP_PRODUCTION_URI', '');