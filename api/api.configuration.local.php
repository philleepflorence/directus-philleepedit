<?php
	
$configuration = [
	"endpoints" => [
		"app" => [
			"tables" => [			
		        "albums"=> [
		            "table" => "app_albums",
		            "params" => [
		                "order" => [
		                    "sort" => "ASC"
		                ]
		            ]
		        ],
		        "analytics"=> [
		            "table" => "app_analytics"
		        ],
		        "configuration"=> [
		            "table" => "app_configuration"
		        ],
		        "dropdowns"=> [
		            "table" => "app_dropdowns",
		            "params" => [
		                "order" => [
		                    "sort" => "ASC"
		                ]
		            ]
		        ],
		        "gallery"=> [
		            "table" => "app_gallery",
		            "params" => [
		                "order" => [
		                    "sort" => "ASC"
		                ]
		            ]
		        ],
		        "icons"=> [
		            "table" => "app_icons"
		        ],
		        "labels"=> [
		            "table" => "app_labels"
		        ],
		        "navigation"=> [
		            "table" => "app_navigation",
		            "params" => [
		                "order" => [
		                    "sort" => "ASC"
		                ]
		            ]
		        ],
		        "pages"=> [
		            "table" => "app_pages",
		            "params" => [
		                "order" => [
		                    "sort" => "ASC"
		                ]
		            ]
		        ],
		        "templates"=> [
		            "table" => "app_templates"
		        ]
			]
		],
		"cdn" => [
			"extensions" => ['gif', 'jpeg', 'jpg', 'png'],
			"sizes" => [				
		        "thumbs" => [
		            "width" => 200,
		            "height" => 200,
		            "offset" => 130,
		            "size" => "thumbs",
		            "device" => "All - Thumbnails",
		            "trim" => 0
		        ],
		        "xs" => [
		            "width" => 480,
		            "height" => 480,
		            "offset" => 130,
		            "size" => "xs",
		            "device" => "Mobile"
		        ],
		        "xs" => [
		            "width" => 480,
		            "height" => 480,
		            "offset" => 130,
		            "size" => "xs",
		            "device" => "Mobile"
		        ],
		        "sm" => [
		            "width" => 640,
		            "height" => 640,
		            "offset" => 180,
		            "size" => "sm",
		            "device" => "Mobile, Tablet"
		        ],
		        "md" => [
		            "width" => 960,
		            "height" => 960,
		            "offset" => 200,
		            "size" => "md",
		            "device" => "Tablet"
		        ],
		        "lg" => [
		            "width" => 1280,
		            "height" => 1280,
		            "offset" => 200,
		            "size" => "lg",
		            "device" => "Laptop, Desktop"
		        ],
		        "xl" => [
		            "width" => 1600,
		            "height" => 1600,
		            "offset" => 150,
		            "size" => "xl",
		            "device" => "Laptop, Desktop, HD"
		        ],
		        "xxl" => [
		            "width" => 1900,
		            "height" => 1900,
		            "offset" => 0,
		            "size" => "xxl",
		            "device" => "Desktop, FHD"
		        ]
			]
		],
		"contents" => [
			"tables" => ["contents_events", "contents_posts"]
		],
		"pages" => [
			"tables" => []
		],
		"rows" => [
			"types" => [
				'INTEGER' => "is_int", 
				'INT' => "is_int", 
				'SMALLINT' => "is_int", 
				'TINYINT' => "is_int", 
				'MEDIUMINT' => "is_int", 
				'BIGINT' => "is_int", 
				'DECIMAL' => "is_float", 
				'NUMERIC' => "is_float", 
				'DECIMAL' => "is_float"
			]
		]
	]
];